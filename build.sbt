name := """Multi-Bible"""

version := "1.2"

scalaVersion := "2.11.2"

scalacOptions in ThisBuild ++= Seq("-feature", "-deprecation", "-unchecked")

libraryDependencies ++= List(
  "org.scalatest" %% "scalatest" % "2.2.1" % "test",
  "org.codehaus.groovy" % "groovy-all" % "2.3.9",
  "com.itextpdf" % "itextpdf" % "5.0.6",
  "org.yaml" % "snakeyaml" % "1.18"
)

resolvers += "ApacheSnapshot" at "https://repository.apache.org/content/groups/snapshots/"
