package multiBible

import multiBible.Parser.NumericExtractor
import org.scalatest.FunSuite

import scala.util.{Failure, Success}

class parser_Suite extends FunSuite {
  test("Simple single line text with sequential numbers is handled without issues") {
    assertSuccessful(
      "1 VerseA 2 VerseB 3 VerseC", Map(
        1 -> "VerseA",
        2 -> "VerseB",
        3 -> "VerseC"
      ),
      numericExtractor = Parser.HinduArabicExtractor
    )
  }

  test("Numbers within words don't get picked up as verse numbers") {
    assertSuccessful(
      "1 Verse2 2 3Verse 3 Ver4se", Map(
        1 -> "Verse2",
        2 -> "3Verse",
        3 -> "Ver4se"
      ),
      numericExtractor = Parser.HinduArabicExtractor
    )
  }

  test("Simple multiline text with sequential numbers are handled and new lines trimmed") {
    assertSuccessful(
      """1 Verse1
         |2 Verse2
         |3 Verse3
      """.stripMargin, Map(
        1 -> "Verse1",
        2 -> "Verse2",
        3 -> "Verse3"
      ),
      numericExtractor = Parser.HinduArabicExtractor
    )
  }

  test("Non-sequential numbers aren't seen as verse markers") {
    assertSuccessful(
      """3 Verse1 300 lambs
        |4 Verse2
      """.stripMargin, Map(
        3 -> "Verse1 300 lambs",
        4 -> "Verse2"
      ),
      numericExtractor = Parser.HinduArabicExtractor
    )
  }

  test("Dari extractor parses successfully and leaves hindu arabic numbers alone") {
    assertSuccessful(
      "۱ verse 1 ۲ verse 2 ۳ verse 3", Map(
        1 -> "verse 1",
        2 -> "verse 2",
        3 -> "verse 3"
      ),
      numericExtractor = Parser.DariExtractor
    )
  }

  test("Dari extractor detects verse markers which cling to the verse coming after") {
    assertSuccessful(
      "۱verse 1 ۲verse 2 ۳verse 3", Map(
        1 -> "verse 1",
        2 -> "verse 2",
        3 -> "verse 3"
      ),
      numericExtractor = Parser.DariExtractor
    )
  }

  test("Dari extractor doesn't count out of sequence numbers as verse markers") {
    assertSuccessful(
      "۴ ۱۱ men ۵ verse 5", Map(
        4 -> "۱۱ men",
        5 -> "verse 5"
      ),
      numericExtractor = Parser.DariExtractor
    )
  }

  private def assertSuccessful(text: String, expectedMap: Map[Int, String], numericExtractor: NumericExtractor): Unit = {
    Parser.tryParse(numericExtractor, text) match {
      case Success(map) => assert(map === expectedMap)
      case Failure(exception) => throw new Exception(s"Unable to parse '$text'", exception)
    }
  }
}
