package scripts

import java.awt.{BorderLayout, Font}
import java.io.File
import javax.swing.{Box, JFrame, JLabel}
import multiBible.UI._

import utils.Script

object FontFinder extends App {
  implicit val scriptArgs = args

  val rootFontsDir = {
    val fontsDirPath = Script.tryFlagValue("fontDir").getOrElse("/usr/share/fonts/truetype/")
    val dir = new File(fontsDirPath)
    if (!dir.exists())
      throw new RuntimeException(s"Nothing found at path: '$fontsDirPath'")
    if (!dir.isDirectory)
      throw new RuntimeException(s"Object at path: '$fontsDirPath' isn't a directory")
    dir
  }

  def findFontFilesInDir(dir: File): Seq[File] = {
    val files = dir.listFiles()
    val subDirs = files.filter(_.isDirectory)
    val fontFiles = files.filter(_.isFile).filter(_.getName.endsWith(".ttf"))

    val subDirFontFiles = subDirs.flatMap(findFontFilesInDir)

    subDirFontFiles ++ fontFiles
  }
  
  case class FontInfo(font: Font, file: File)

  val allFontInfo = findFontFilesInDir(rootFontsDir).map {
    case fontFile => FontInfo(Font.createFont(Font.TRUETYPE_FONT, fontFile), fontFile)
  }

  val frame = new JFrame {
    private var _testText: String = null

    private val _resultsBox = Box.createVerticalBox()

    getContentPane.add(BorderLayout.NORTH, build[Box](
      Box.createVerticalBox(),

      _.add { build[Box](
        Box.createHorizontalBox(),
        _.add { labelledTextBox("Test Text", _testText = _) },
        _.add { button("Search", 'S', _calculateAndUpdateMatches())}

      )},
      _.add(_resultsBox)
    ))

    private def _calculateAndUpdateMatches(): Unit = {
      val matchingFontInfo = allFontInfo.filter(fontInfo => _testText.forall(char => fontInfo.font.canDisplay(char)))

      _resultsBox.removeAll()

      matchingFontInfo.zipWithIndex.map {
        case (FontInfo(baseFont, file), index) =>
          val largerFont = baseFont.deriveFont(Font.PLAIN, 24)
          val label = new JLabel(s"[$index] ${baseFont.getFontName} : ${_testText} (${file.getPath})")
          label.setFont(largerFont)
          _resultsBox.add(label)
      }

      this.revalidate()
      this.repaint()
    }

    setSize(700, 800)
    setVisible(true)
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
  }
}
