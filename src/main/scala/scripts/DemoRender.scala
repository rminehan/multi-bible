package scripts

import multiBible.Interop.FontSize
import multiBible.PassageCache.Passage
import multiBible._
import multiBible.pdf.PdfGenerator

object DemoRender extends App {
  val passageCache = new PassageCache

  object Passages {
    lazy val matthew = Passage(Books.matthew, 7, 1, 12)
    lazy val john = Passage(Books.john, 3, 1, 3)

    lazy val all = Seq(matthew, john)
  }

  def filePath(stem: String, fontSize: FontSize): String  = s"generated/.DemoRenderer/$stem-$fontSize.pdf"

  object NIV84 {
    val version = UI.bridgeVersion
    
    passageCache.add(Passages.matthew.chapterReference(version), verses = Seq(
      1 -> "Do not judge, or you too will be judged.",
      2 -> "For in the same way you judge others, you will be judged, and with the measure you use, it will be measured to you.",
      3 -> "Why do you look at the speck of sawdust in your brother's eye and pay no attention to the plank in your own eye?",
      4 -> "How can you say to your brother, 'Let me take the speck out of your eye,' when all the time there is a plank in your own eye?",
      5 -> "You hypocrite, first take the plank out of your own eye, and then you will see clearly to remove the speck from your brother's eye.",
      6 -> "Do not give dogs what is sacred; do not throw your pearls to pigs. If you do, they may trample them under their feet, and turn and tear you to pieces.",
      7 -> "Ask and it will be given to you; seek and you will find; knock and the door will be opened to you.",
      8 -> "For everyone who asks receives; the one who seeks finds; and to the one who knocks, the door will be opened.",
      9 -> "Which of you, if your son asks for bread, will give him a stone?",
      10 -> "Or if he asks for a fish, will give him a snake?",
      11 -> "If you, then, though you are evil, know how to give good gifts to your children, how much more will your Father in heaven give good gifts to those who ask him!",
      12 -> "So in everything, do to others what you would have them do to you, for this sums up the Law and the Prophets."
    ))
    
    passageCache.add(Passages.john.chapterReference(version), verses = Seq(
      1 -> "Now there was a man of the Pharisees named Nicodemus, a member of the Jewish ruling council.",
      2 -> "He came to Jesus at night and said, 'Rabbi, we know you are a teacher who has come from God. For no one could perform the miraculous signs you are doing if God were not with him.'",
      3 -> "In reply Jesus declared, 'I tell you the truth, no one can see the kingdom of God unless he is born again.'"
    ))
  }

  PdfGenerator.generatePdf(
    fontSize = 18f,
    passages = Passages.all,
    versions = Seq(NIV84.version),
    verseMap = passageCache.toMap,
    path = filePath("niv", 18f)
  )

  object Chinese_ERV {
    val version = Plugins.versions.find(_.description == "Chinese ERV").get
    
    passageCache.add(Passages.matthew.chapterReference(version), verses = Seq(
      1 -> "'不要评判人，上帝就不会评判你们。",
      2 -> "因为你们用什么样的方式评判人，上帝也会用同样的方式来评判你们。你们用什么尺度衡量人，上帝也会用同样的尺度来衡量你们。",
      3 -> "'为什么你只看见朋友眼里有刺，却看不见自己眼里有梁木呢？",
      4 -> "既然你眼里有梁木，怎么能对你的朋友说∶'让我来把你眼中的刺挑出来'呢？",
      5 -> "你这个虚伪的人啊，还是先移去你自己眼中的梁木吧，然后，你才能看清楚，把朋友眼里的刺挑出来。",
      6 -> "'不要把圣物喂狗，狗会反咬你一口；也不要把珍珠丢给猪，它们只会用蹄子践踏了珍珠。",
      7 -> "'不断地请求，上帝就会赐给你们；不断地寻求，你们就会找到；不停地敲门，门就会为你们打开。",
      8 -> "是的，不断请求的人就会得到；不断寻找的人就会找到；不停敲门的人，门就会为他打开。",
      9 -> "'你们当中谁有儿子吗？如果他向你要面包，你会给他石头吗？",
      10 -> "或者，如果他要鱼，你会给他蛇吗？绝对不会！",
      11 -> "你们虽然邪恶，但是还知道如何把好东西给自己的孩子，那么，你们的天父肯定会把好东西给那些向他请求的人们了！",
      12 -> "'因此，在任何事情上，你们想让别人怎样对待自己，你们也应该怎样对待别人，这就是摩西律法和先知教导的含义。"
    ))

    passageCache.add(Passages.john.chapterReference(version), verses = Seq(
      1 -> "有个法利赛人，名叫尼哥底母，他是犹太人的一位首领",
      2 -> "夜里，他来见耶稣，说∶“拉比（老师），我们知道您是来自上帝的老师，因为假如上帝不与您同在，没人能行您行的神迹。”",
      3 -> "耶稣回答道∶“我实话告诉你，如果一个人不重生，就不能在上帝的王国占有一席之地。”"
    ))
  }

  PdfGenerator.generatePdf(
    fontSize = 14f,
    passages = Seq(Passages.matthew),
    versions = Seq(Chinese_ERV.version, NIV84.version),
    verseMap = passageCache.toMap,
    path = filePath("chinese_english", 14f)
  )

  object FarsiWordProject {
    val version = Plugins.versions.find(_.description == "Persian").get
    
    passageCache.add(Passages.matthew.chapterReference(version), verses = Seq(
      1 -> "حکم مکنید تا بر شما حکم نشود.",
      2 -> "زیرا بدان طریقی که حکم کنید بر شما نیز حکم خواهد شد و بدان پیمانهای که پیمایید برای شما خواهند پیمود.",
      3 -> "و چون است که خس را در چشم برادر خود می‌بینی و چوبی را که در چشم خود داری نمی‌یابی؟ ",
      4 -> "یا چگونه به برادر خود می‌گویی، اجازت ده تا خس را از چشمت بیرون کنم، و اینک، چوب در چشم تو است؟ ",
      5 -> "ای ریاکار، اوّل چوب را از چشم خود بیرون کن، آنگاه نیک خواهی دید تا خس را از چشم برادرت بیرون کنی! ",
      6 -> "آنچه مقدّس است، به سگان مدهید و نه مرواریدهای خود را پیش گرازان اندازید، مبادا آنها را پایمال کنند و برگشته، شما را بدرند. ",
      7 -> "سؤال کنید که به شما داده خواهد شد؛ بطلبید که خواهید یافت؛ بکوبید که برای شما باز کرده خواهد شد. ",
      8 -> "زیرا هر که سؤال کند، یابد و کسی که بطلبد، دریافت کند و هر که بکوبد برای او گشاده خواهد شد. ",
      9 -> "و کدام آدمی است از شما که پسرش نانی از او خواهد و سنگی بدو دهد؟ ",
      10 -> " یا اگر ماهی خواهد ماری بدو بخشد؟ ",
      11 -> " پس هرگاه شما که شریر هستید، دادن بخششهای نیکو را به اولاد خود می‌دانید، چقدر زیاده پدر شما که در آسمان است چیزهای نیکو را به آنانی که از او سؤال می‌کنند خواهد بخشید! ",
      12 -> " له'ذا آنچه خواهید که مردم به شما کنند، شما نیز بدیشان همچنان کنید؛ زیرا این است تورات و صُحُف انبیا.  "
    ))
    
    passageCache.add(Passages.john.chapterReference(version), verses = Seq(
      1 -> "و شخصی از فریسیان نیقودیموس نام از رؤسای یهود بود.",
      2 -> "او در شب نزد عیسی آمده، به وی گفت، ای استاد می‌دانیم که تو معلّم هستی که از جانب خدا آمده‌ای زیرا هیچ کس نمی‌تواند معجزاتی را که تو می‌نمایی بنماید، جز اینکه خدا با وی باشد.",
      3 -> "عیسی در جواب او گفت، آمین آمین به تو می‌گویم اگر کسی از سرِ نو مولود نشود، ملکوت خدا را نمی‌تواند دید."
    ))
  }

  PdfGenerator.generatePdf(
    fontSize = 17f,
    passages = Passages.all.reverse,
    versions = Seq(FarsiWordProject.version, Chinese_ERV.version, NIV84.version),
    verseMap = passageCache.toMap,
    path = filePath("persian_chinese_english", 17f)
  )

  PdfGenerator.generatePdf(
    fontSize = 17f,
    passages = Passages.all.reverse,
    versions = Seq(FarsiWordProject.version, Chinese_ERV.version),
    verseMap = passageCache.toMap,
    path = filePath("persian_chinese", 17f)
  )
}
