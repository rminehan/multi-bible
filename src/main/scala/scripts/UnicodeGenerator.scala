package scripts

import java.awt.BorderLayout
import javax.swing.{Box, JLabel, JFrame}
import multiBible.UI._
import utils.{Clipboard, Log}

object UnicodeGenerator extends App {
  val frame = new JFrame {
    private var _unicodePoint: String = null

    private val _resultsLabel = new JLabel

    getContentPane.add(BorderLayout.NORTH, build[Box](
      Box.createHorizontalBox(),

      _.add { labelledTextBox("Enter unicode in hex", _unicodePoint = _) },
      _.add { button("Convert", 'C', _convertUnicode())},
      _.add(_resultsLabel)
    ))

    private def _convertUnicode(): Unit = {
      val codePoint = Integer.parseInt(_unicodePoint, 16)
      val char = codePoint.asInstanceOf[Char]
      val name = Character.getName(codePoint)
      _resultsLabel.setText(s"  $name:  $char  ")
      Clipboard.set(char.toString)
      Log.message(s"Copied char #${_unicodePoint} ($name) to clipboard - rendered as $char")
      this.revalidate()
      this.repaint()
    }

    setSize(600, 50)
    setVisible(true)
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
  }
}
