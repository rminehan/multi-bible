package scripts

import multiBible.Interop.FontSize
import multiBible.pdf.PdfGenerator
import multiBible.{Version, Books, PassageCache}
import multiBible.Plugins.findVersion
import multiBible.PassageCache.{NumberedVerses, Passage}

/** Script to test the core software on a miscellaneous passage */
object MiscPassage extends App {

  val passageCache = new PassageCache

  object Passages {
    val genesis15 = Passage(Books.genesis, 15, 6, 6)
    val joshua24 = Passage(Books.joshua, 24, 1, 3)
    val genesis17 = Passage(Books.genesis, 17, 19, 22)
    
    val all = Seq(genesis15, joshua24, genesis17)
  }

  val niv = findVersion("NIV")

  def path(fileStem: String): String = s"generated/.misc/$fileStem.pdf"
  def versionPath(version: Version): String = path(version.fileStem)

  def generateInterlinear(fontSize: FontSize, leadVersion: Version): Unit = PdfGenerator.generatePdf(
    fontSize = fontSize,
    passages = Passages.all,
    versions = Seq(leadVersion, niv),
    verseMap = passageCache.toMap,
    path = versionPath(leadVersion)
  )

  def add(version: Version, genesis15Verses: NumberedVerses, joshua24Verses: NumberedVerses, genesis17Verses: NumberedVerses): Unit = {
    passageCache.add(Passages.genesis15.chapterReference(version), verses = genesis15Verses)
    passageCache.add(Passages.joshua24.chapterReference(version), verses = joshua24Verses)
    passageCache.add(Passages.genesis17.chapterReference(version), verses = genesis17Verses)
  }

  // =========== NIV ================
  add(niv, Seq(
      6 -> "Abram believed the LORD, and he credited it to him as righteousness."
    ), Seq(
      1 -> "Then Joshua assembled all the tribes of Israel at Shechem. He summoned the elders, leaders, judges and officials of Israel, and they presented themselves before God.",
      2 -> """Joshua said to all the people, "This is what the LORD, the God of Israel, says: 'Long ago your forefathers, including Terah the father of Abraham and Nahor, lived beyond the River and worshiped other gods.""",
      3 -> "But I took your father Abraham from the land beyond the River and led him throughout Canaan and gave him many descendants. I gave him Isaac,"
    ), Seq(
      19 -> """Then God said, "Yes, but your wife Sarah will bear you a son, and you will call him Isaac. I will establish my covenant with him as an everlasting covenant for his descendants after him.""",
      20 -> "And as for Ishmael, I have heard you: I will surely bless him; I will make him fruitful and will greatly increase his numbers. He will be the father of twelve rulers, and I will make him into a great nation.",
      21 -> "But my covenant I will establish with Isaac, whom Sarah will bear to you by this time next year.",
      22 -> "When he had finished speaking with Abraham, God went up from him."
    )
  )

  PdfGenerator.generatePdf(
    fontSize = 19,
    passages = Passages.all,
    versions = Seq(niv),
    verseMap = passageCache.toMap,
    path = versionPath(niv)
  )

  // =========== CNTS ================
  val cnvs = findVersion("cnvs")
  add(cnvs, Seq(
      6 -> "亚伯兰信耶和华，耶和华就以此算为他的义了。"
    ), Seq(
      1 -> "约书亚在示剑召集以色列各支派，又把以色列的长老、首领、审判官和官长都召了来，他们就站在　神面前。",
      2 -> "约书亚对众民说:“耶和华以色列的 神这样说 :‘古时你们的列祖,就是亚伯拉罕和拿鹤的父亲他 拉,住在大河那边;他们事奉别的神。",
      3 -> "我把你们的祖先亚伯拉罕,从大河那边带来,领 他走遍迦南地,使他的后裔增多,把以撒赐给他;"
    ), Seq(
      19 -> "神说:“你的妻子撒拉,真的要为你生一个儿 子,你要给他起名叫以撒,我要与他坚立我的约 ,作他后裔的永约。",
      20 -> "至于以实玛利,我也应允你。看哪,我已经赐福 给他;我必使他昌盛,子孙极其众多;他必生十二 个族长;我也必使他成为大国。",
      21 -> "但我的约是要和以撒坚立的。这以撒,就是明年 这时候,撒拉要为你生的。”",
      22 -> "神和亚伯拉罕说完了话,就离开他上升去了。"
    )
  )
  generateInterlinear(fontSize = 19, cnvs)

  // =========== CNVS ================
  val cnvt = findVersion("cnvt")
  add(cnvt, Seq(
      6 -> "亞伯蘭信耶和華，耶和華就以此算為他的義了。"
    ), Seq(
      1 -> "約書亞在示劍召集以色列各支派,又把以色列的 長老、首領、審判官和官長都召了來,他們就站在 神面前。",
      2 -> "約書亞對眾民說:“耶和華以色列的 神這樣說 :‘古時你們的列祖,就是亞伯拉罕和拿鶴的父親他 拉,住在大河那邊;他們事奉別的神。",
      3 -> "我把你們的祖先亞伯拉罕,從大河那邊帶來,領 他走遍迦南地,使他的後裔增多,把以撒賜給他;"
    ), Seq(
      19 -> "神說:“你的妻子撒拉,真的要為你生一個兒 子,你要給他起名叫以撒,我要與他堅立我的約 ,作他後裔的永約。",
      20 -> "至於以實瑪利,我也應允你。看哪,我已經賜福 給他;我必使他昌盛,子孫極其眾多;他必生十二 個族長;我也必使他成為大國。",
      21 -> "但我的約是要和以撒堅立的。這以撒,就是明年 這時候,撒拉要為你生的。”",
      22 -> "神和亞伯拉罕說完了話,就離開他上升去了。"
    )
  )
  generateInterlinear(fontSize = 19, cnvt)

  // =========== Dari ================
  val dari = findVersion("dari")
  add(dari, Seq(
      6 -> "ابرام به خداوند ایمان آورد و خداوند این را برای او عدالت شمرد و او را قبول درگاه خود کرد."
    ), Seq(
      1 -> "بعد یوشع تمام قبایل اسرائیل را با مو سفیدان، سرکردگان، قاضیان و مأمورین شان در شکیم فراخواند. آن ها آمدند و به پیشگاه خدا حاضر شدند.",
      2 -> "یوشع به آن ها گفت :  «خداوند، خدای بنی اسرائیل چنین می فرماید :  سالها پیش اجداد شما در آن طرف دریای فرات زندگی می کردند و خدایان بیگانه را می پرستیدند. یکی از اجداد تان، طارح پدر ابراهیم و ناحور بود.",
      3 -> "عد جد تان، ابراهیم را از سرزمین آنطرف دریای فرات به سراسر کنعان هدایت نمودم. اولادۀ او را زیاد کردم و اسحاق را به او دادم."
    ), Seq(
      19 -> "خدا فرمود :  «نی، زن تو ساره پسری برای تو به دنیا می آورد، اسم او را اسحاق می گذاری. من پیمان خود را با او برای همیشه حفظ می کنم. این یک پیمان جاودانی است.",
      20 -> "من شنیدم که تو دربارۀ اسماعیل درخواست نمودی، بنابراین، من او را برکت می دهم و به او فرزندان بسیار و نسل های زیاد می دهم. او پدر دوازده پادشاه می شود. من ملت بزرگی از نسل او به وجود می آورم.",
      21 -> "اما پیمان خود را با پسر تو اسحاق حفظ می کنم. او سال دیگر در همین وقت توسط ساره به دنیا می آید.»",
      22 -> "وقتی خدا گفتگوی خود را با ابراهیم تمام کرد، از نزد او رفت."
    )
  )
  generateInterlinear(fontSize = 19, dari)

  // =========== ESV ================
  val esv = findVersion("esv")
  add(esv, Seq(
      6 -> "And he believed the Lord, and he counted it to him as righteousness."
    ), Seq(
      1 -> "Joshua gathered all the tribes of Israel to Shechem and summoned the elders, the heads, the judges, and the officers of Israel. And they presented themselves before God.",
      2 -> "And Joshua said to all the people, “Thus says the Lord, the God of Israel, ‘Long ago, your fathers lived beyond the Euphrates,[a] Terah, the father of Abraham and of Nahor; and they served other gods.",
      3 -> "Then I took your father Abraham from beyond the River and led him through all the land of Canaan, and made his offspring many. I gave him Isaac."
    ), Seq(
      19 -> "God said, “No, but Sarah your wife shall bear you a son, and you shall call his name Isaac. I will establish my covenant with him as an everlasting covenant for his offspring after him.",
      20 -> "As for Ishmael, I have heard you; behold, I have blessed him and will make him fruitful and multiply him greatly. He shall father twelve princes, and I will make him into a great nation.",
      21 -> "But I will establish my covenant with Isaac, whom Sarah shall bear to you at this time next year.”",
      22 -> "When he had finished talking with him, God went up from Abraham."
    )
  )

  // =========== WLC ================
  val wlc = findVersion("hebrew")
  add(wlc, Seq(
      6 -> "וְהֶאֱמִ֖ן בַּֽיהוָ֑ה וַיַּחְשְׁבֶ֥הָ לּ֖וֹ צְדָקָֽה׃"
    ), Seq(
      1 -> "וַיֶּאֶסֹ֧ף יְהוֹשֻׁ֛עַ אֶת־כָּל־שִׁבְטֵ֥י יִשְׂרָאֵ֖ל שְׁכֶ֑מָה וַיִּקְרָא֩ לְזִקְנֵ֨י יִשְׂרָאֵ֜ל וּלְרָאשָׁ֗יו וּלְשֹֽׁפְטָיו֙ וּלְשֹׁ֣טְרָ֔יו וַיִּֽתְיַצְּב֖וּ לִפְנֵ֥י הָאֱלֹהִֽים׃",
      2 -> "וַיֹּ֨אמֶר יְהוֹשֻׁ֜עַ אֶל־כָּל־הָעָ֗ם כֹּֽה־אָמַ֣ר יְהוָה֮ אֱלֹהֵ֣י יִשְׂרָאֵל֒ בְּעֵ֣בֶר הַנָּהָ֗ר יָשְׁב֤וּ אֲבֽוֹתֵיכֶם֙ מֵֽעוֹלָ֔ם תֶּ֛רַח אֲבִ֥י אַבְרָהָ֖ם וַאֲבִ֣י נָח֑וֹר וַיַּעַבְד֖וּ אֱלֹהִ֥ים אֲחֵרִֽים׃",
      3 -> "וָ֠אֶקַּח אֶת־אֲבִיכֶ֤ם אֶת־אַבְרָהָם֙ מֵעֵ֣בֶר הַנָּהָ֔ר וָאוֹלֵ֥ךְ אוֹת֖וֹ בְּכָל־אֶ֣רֶץ כְּנָ֑עַן ׳וָאֶרֶב׳ ״וָאַרְבֶּה֙״ אֶת־זַרְע֔וֹ וָֽאֶתֶּן־ל֖וֹ אֶת־יִצְחָֽק׃"
    ), Seq(
      19 -> "וַיֹּ֣אמֶר אֱלֹהִ֗ים אֲבָל֙ שָׂרָ֣ה אִשְׁתְּךָ֗ יֹלֶ֤דֶת לְךָ֙ בֵּ֔ן וְקָרָ֥אתָ אֶת־שְׁמ֖וֹ יִצְחָ֑ק וַהֲקִמֹתִ֨י אֶת־בְּרִיתִ֥י אִתּ֛וֹ לִבְרִ֥ית עוֹלָ֖ם לְזַרְע֥וֹ אַחֲרָֽיו׃",
      20 -> "וּֽלְיִשְׁמָעֵאל֮ שְׁמַעְתִּיךָ֒ הִנֵּ֣ה׀ בֵּרַ֣כְתִּי אֹת֗וֹ וְהִפְרֵיתִ֥י אֹת֛וֹ וְהִרְבֵּיתִ֥י אֹת֖וֹ בִּמְאֹ֣ד מְאֹ֑ד שְׁנֵים־עָשָׂ֤ר נְשִׂיאִם֙ יוֹלִ֔יד וּנְתַתִּ֖יו לְג֥וֹי גָּדֽוֹל׃",
      21 -> "וְאֶת־בְּרִיתִ֖י אָקִ֣ים אֶת־יִצְחָ֑ק אֲשֶׁר֩ תֵּלֵ֨ד לְךָ֤ שָׂרָה֙ לַמּוֹעֵ֣ד הַזֶּ֔ה בַּשָּׁנָ֖ה הָאַחֶֽרֶת׃",
      22 -> "וַיְכַ֖ל לְדַבֵּ֣ר אִתּ֑וֹ וַיַּ֣עַל אֱלֹהִ֔ים מֵעַ֖ל אַבְרָהָֽם׃"
    )
  )
  PdfGenerator.generatePdf(
    fontSize = 19,
    passages = Passages.all,
    versions = Seq(wlc, esv),
    verseMap = passageCache.toMap,
    path = versionPath(wlc)
  )

  // =========== korean ================
  val korean = findVersion("korean")
  add(korean, Seq(
      6 -> "아브람이 여호와를 믿으니 여호와께서 이를 그의 의로 여기시고"
    ), Seq(
      1 -> "여호수아가 이스라엘 모든 지파를 세겜에 모으고 이스라엘 장로들과 그 두령들과 재판장들과 유사들을 부르매 그들이 하나님 앞에 보인지라",
      2 -> """"여호수아가 모든 백성에게 이르되 이스라엘 하나님 여호와의 말씀에 옛적에 너희 조상들 곧 아브라함의 아비, 나홀의 아비 데라가 강 저편에 거하여 다른 신들을 섬겼으나"""",
      3 -> "내가 너희 조상 아브라함을 강 저편에서 이끌어내어 가나안으로 인도하여 온 땅을 두루 행하게 하고 그 씨를 번성케 하려고 그에게 이삭을 주었고"
    ), Seq(
      19 -> "하나님이 가라사대 아니라 네 아내 사라가 정녕 네게 아들을 낳으리니 너는 그 이름을 이삭이라 하라 내가 그와 내 언약을 세우리니 그의 후손에게 영원한 언약이 되리라",
      20 -> "이스마엘에게 이르러는 내가 네 말을 들었나니 내가 그에게 복을주어 생육이 중다하여 그로 크게 번성케 할지라 그가 열 두 방백을 낳으리니 내가 그로 큰 나라가 되게 하려니와",
      21 -> "내 언약은 내가 명년 이 기한에 사라가 네게 낳을 이삭과 세우리라",
      22 -> "하나님이 아브라함과 말씀을 마치시고 그를 떠나 올라가셨더라"
    )
  )
  generateInterlinear(fontSize = 19, korean)

  // =========== persian ================
  val persian = findVersion("persian")
  add(persian, Seq(
      6 -> "و به‌ خداوند ایمان‌ آورد، و او، این‌ را برای‌ وی‌ عدالت‌ محسوب‌ كرد."
    ), Seq(
      1 -> "و یوشع‌ تمامی‌ اسباط‌ اسرائیل‌ را در شكیم‌ جمع‌ كرد، و مشایخ‌ اسرائیل‌ و رؤسا و داوران‌ و ناظران‌ ایشان‌ را طلبیده‌، به‌ حضور خدا حاضر شدند.",
      2 -> "و یوشع‌ به‌ تمامی‌ قوم‌ گفت‌ كه‌ «یهُوَه‌ خدای‌ اسرائیل‌ چنین‌ می‌گوید كه‌ پدران‌ شما، یعنی‌ طارح‌ پدر ابراهیم‌ و پدر ناحور، در زمان‌ قدیم‌ به‌ آن‌ طرف‌ نهر ساكن‌ بودند، و خدایان‌ غیر را عبادت‌ می‌نمودند. ",
      3 -> " و پدر شما ابراهیم‌ را از آن‌ طرف‌ نهر گرفته‌، در تمامی‌ زمین‌ كنعان‌ گردانیدم‌، و ذریت‌ او را زیاد كردم‌ و اسحاق‌ را به‌ او دادم‌."
    ), Seq(
      19 -> "خدا گفت‌: «به‌ تحقیق‌ زوجه‌ات‌ ساره‌ برای‌ تو پسری‌ خواهد زایید، و او را اسحاق‌ نام‌ بنه‌، و عهد خود را با وی‌ استوار خواهم‌ داشت‌، تا با ذریت‌ او بعد از او عهد ابدی‌ باشد. ",
      20 -> "و اما در خصوص‌اسماعیل‌، تو را اجابت‌ فرمودم‌. اینك‌ او را بركت‌ داده‌، بارور گردانم‌، و او را بسیار كثیر گردانم‌. دوازده‌ رئیس‌ از وی‌ پدید آیند، و امتی‌ عظیم‌ از وی‌ بوجود آورم‌. ",
      21 -> "لكن‌ عهد خود را با اسحاق‌ استوار خواهم‌ ساخت‌، كه‌ ساره‌ او را بدین‌ وقت‌ در سال‌ آینده‌ برای‌ تو خواهد زایید.»",
      22 -> "و چون‌ خدا از سخن‌ گفتن‌ با وی‌ فارغ‌ شد، از نزد ابراهیم‌ صعود فرمود."
    )
  )
  generateInterlinear(fontSize = 17, persian)

  // =========== tamil ================
  val tamil = findVersion("tamil")
  add(tamil, Seq(
      6 -> "ஆபிராம் தேவனை நம்பினான். மேலும் தேவன் ஆபிராமின் நம்பிக்கையை அவனுடைய நீதியான காரியமாக எண்ணினார்."
    ), Seq(
      1 -> "இஸ்ரவேல் கோத்திரத்தினர் எல்லோரையும் சீகேமில் கூடுமாறு யோசுவா அழைத்தான். பின் யோசுவா மூத்த தலைவர்களையும், நியாயாதிபதிகளையும், அதிகாரிகளையும் இஸ்ரவேலின் ஆட்சியாளரையும் அழைத்தான். இவர்கள் தேவனுக்கு முன்னே நின்றார்கள்.",
      2 -> "பின்பு யோசுவா எல்லா ஜனங்களையும் பார்த்துப் பேசினான். அவன், “இஸ்ரவேலின் தேவனாகிய கர்த்தர், உங்களுக்குக் கூறுவதை நான் சொல்லுகிறேன்: ‘பலகாலத்திற்கு முன், உங்கள் முற்பிதாக்கள் ஐபிராத்து நதியின் மறுபுறத்தில் வாழ்ந்தனர். ஆபிரகாம், நாகோர் ஆகியோரின் தந்தையாகிய தேராகு, போன்றோரைக் குறித்து நான் பேசிக் கொண்டிருக்கிறேன். அப்போது, அம்மனிதர்கள் வேறு தெய்வங்களை வணங்கி வந்தனர்.",
      3 -> "ஆனால் கர்த்தராகிய நான், நதிக்கு மறுபுறத்திலுள்ள தேசத்திலிருந்து உங்கள் தந்தையாகிய ஆபிரகாமை அழைத்து வந்தேன். கானான் தேசத்தின் வழியாக அவனை வழிநடத்திப் பல பல பிள்ளைகளை அவனுக்குக் கொடுத்தேன். ஆபிரகாமுக்கு ஈசாக்கு என்னும் பெயருள்ள மகனைக் கொடுத்தேன்."
    ), Seq(
      19 -> "தேவன், “இல்லை. உன் மனைவி சாராள் ஒரு மகனைப் பெறுவாள் என்று சொன்னேன். நீ அவனுக்கு ஈசாக்கு என்று பெயரிடுவாய். நான் அவனோடு என் உடன்படிக்கையை ஏற்படுத்திக்கொள்வேன். அந்த உடன்படிக்கையே என்றென்றைக்கும் அவனுக்கும் அவனது சந்ததிக்கும் தொடரும்.",
      20 -> "“நீ இஸ்மவேலைப்பற்றிச் சொன்னாய். நான் அவனையும் ஆசீர்வதிப்பேன். அவனுக்கும் நிறைய பிள்ளைகள் இருக்கும். அவன் 12 பெரிய தலைவர்களுக்குத் தந்தையாவான். அவனது குடும்பமே ஒரு நாடாகும்.",
      21 -> "ஆனால் நான் என் உடன்படிக்கையை ஈசாக்கிடம் ஏற்படுத்துவேன். ஈசாக்கு சாராளின் மகனாயிருப்பான். அவன் அடுத்த ஆண்டு இதே நேரத்தில் பிறந்திருப்பான்” என்றார்.",
      22 -> "ஆபிரகாமிடம் தேவன் பேசி முடித்த பிறகு தேவன் அவனை விட்டு விலகிப் போனார்."
    )
  )
  generateInterlinear(fontSize = 18, tamil)
}
