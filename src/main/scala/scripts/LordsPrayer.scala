package scripts

import multiBible.Interop.FontSize
import multiBible.pdf.PdfGenerator
import multiBible.{Version, Books, PassageCache}
import multiBible.Plugins.findVersion
import multiBible.PassageCache.{NumberedVerses, Passage}

object LordsPrayer extends App {

  val passageCache = new PassageCache

  val passage = Passage(Books.matthew, 6, 9, 13)

  def path(fileStem: String): String = s"generated/.misc/$fileStem.pdf"
  def versionPath(version: Version): String = path(version.fileStem)

  def generateLordsPrayer(fontSize: FontSize, version: Version, verses: NumberedVerses): Unit = {
    passageCache.add(passage.chapterReference(version), verses)
    PdfGenerator.generatePdf(
      fontSize = fontSize,
      passages = Seq(passage),
      versions = Seq(version),
      verseMap = passageCache.toMap,
      path = versionPath(version)
    )
  }

  val niv = findVersion("NIV")
  generateLordsPrayer(fontSize = 19, version = niv, verses = Seq(
    9 -> "'Our Father in heaven, hallowed be your name,",
    10 -> "your kingdom come, your will be done on earth as it is in heaven.",
    11 -> "Give us today our daily bread.",
    12 -> "Forgive us our debts, as we also have forgiven our debtors.",
    13 -> "And lead us not into temptation, but deliver us from the evil one.'"
  ))

  val cnvs = findVersion("cnvs")
  generateLordsPrayer(fontSize = 18, version = cnvs, verses = Seq(
    9 -> "‘我们在天上的父，愿你的名被尊为圣，",
    10 -> "愿你的国降临，愿你的旨意成就在地上， 如同在天上一样。",
    11 -> "我们每天所需的食物， 求你今天赐给我们；",
    12 -> "赦免我们的罪债，好象我们饶恕了得罪我们的人；",
    13 -> "不要让我们陷入试探， 救我们脱离那恶者。’"
  ))

  val cnvt = findVersion("cnvt")
  generateLordsPrayer(fontSize = 19, version = cnvt, verses = Seq(
    9 -> "‘我們在天上的父，願你的名被尊為聖，",
    10 -> "願你的國降臨，願你的旨意成就在地上，如同在天上一樣。",
    11 -> "我們每天所需的食物，求你今天賜給我們；",
    12 -> "赦免我們的罪債，好像我們饒恕了得罪我們的人；",
    13 -> "不要讓我們陷入試探，救我們脫離那惡者。’"
  ))

  val dari = findVersion("dari")
  generateLordsPrayer(fontSize = 19, version = dari, verses = Seq(
    9 -> "«ای پدر آسمانی ما، نام تو مقدس باد.",
    10 -> "پادشاهی تو بیاید. ارادۀ تو همانطور که در آسمان اجرا می شود، در زمین نیز اجرا شود.",
    11 -> "نان روزانۀ ما را امروز به ما بده.",
    12 -> "خطایای ما را ببخش، چنانکه ما نیز کسانی را که به ما خطا کرده اند می بخشیم.",
    13 -> "ما را از وسوسه ها دور نگه دار و از شریر رهایی ده، زیرا پادشاهی و قدرت و جلال تا ابدالآباد از تو است. آمین.»"
  ))

  val korean = findVersion("korean")
  // TODO - this doesn't look right - cf http://nonharmingministries.com/lords-prayer/the-lords-prayer-in-korean/
  generateLordsPrayer(fontSize = 19, version = korean, verses = Seq(
    9 -> "하늘에 계신 우리 아버지여 이름이 거룩히 여김을 받으시오며 ",
    10 -> "나라이 임하옵시며 뜻이 하늘에서 이룬 것같이 땅에서도 이루어지이다",
    11 -> "오늘날 우리에게 일용할 양식을 주옵시고",
    12 -> "우리가 우리에게 죄 지은 자를 사하여 준것 같이 우리 죄를 사하여 주옵시고",
    13 -> "우리를 시험에 들게 하지 마옵시고 다만 악에서 구하옵소서"
  ))

  val persian = findVersion("persian")
  generateLordsPrayer(fontSize = 17, version = persian, verses = Seq(
    9 -> "ای پدر ما که در آسمانی، نام تو مقدّس باد.",
    10 -> "ملکوت تو بیاید. ارادهٔ تو چنانکه در آسمان است، بر زمین نیز کرده شود.",
    11 -> "نان کفاف ما را امروز به ما بده.",
    12 -> "و قرضهای ما را ببخش چنانکه ما نیز قرضداران خود را می‌بخشیم.",
    13 -> "و ما را در آزمایش میاور، بلکه از شریر ما را رهایی ده. زیرا ملکوت و قوّت و جلال تا ابدالآباد از آن تو است، آمین."
  ))

  val singhalese = findVersion("singhalese")
  generateLordsPrayer(fontSize = 18, version = singhalese, verses = Seq(
    9 -> "'ස්වර්ගයෙහි වැඩ වසන අපගේ පියාණෙනි, ඔබ වහන්සේගේ නාමයට ගෞරව වේ වා,",
    10 -> "ඔබ වහන්සේගේ රාජ්‍යය පැමිණේ වා, ඔබ වහන්සේගේ කැමැත්ත ඉටු වේ වා, ස්වර්ගයෙහි මෙන් පොළොවෙහි ද එසේ ම වේ වා.",
    11 -> "අපගේ දවස්පතා ආහාරය අද අපට දුන මැනව.",
    12 -> "අපට වරද කළ අයට අප කමා කළාක් මෙන් අපේ වරදට අපට කමා වුව මැනව.",
    13 -> "අප පරීක්ෂාවට නොපමුණුවා, නපුරාගෙන් අප මිදුව මැනව."
  ))

  val tamil = findVersion("tamil")
  generateLordsPrayer(fontSize = 18, version = tamil, verses = Seq(
    9 -> "‘பரலோகத்திலிருக்கும் எங்கள் பிதாவே, உமது பெயர் என்றென்றும் புனிதமாயிருக்கப் பிரார்த்திக்கிறோம்.",
    10 -> "உமது இராஜ்யம் வரவும் பரலோகத்தில் உள்ளது போலவே பூமியிலும் நீர் விரும்பியவை செய்யப்படவும் பிரார்த்திக்கிறோம்.",
    11 -> "ஒவ்வொரு நாளும் எங்களுக்குத் தேவையான உணவை எங்களுக்கு அளிப்பீராக.",
    12 -> "மற்றவர் செய்த தீமைகளை நாங்கள் மன்னித்தது போலவே எங்கள் குற்றங்களையும் மன்னியும்.",
    13 -> "எங்களைச் சோதனைக்கு உட்படப் பண்ணாமல் பிசாசினிடமிருந்து காப்பாற்றும்.’"
  ))
}
