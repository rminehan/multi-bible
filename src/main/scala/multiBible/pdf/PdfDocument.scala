package multiBible.pdf

import java.io.FileOutputStream

import com.itextpdf.text._
import com.itextpdf.text.pdf.{MultiColumnText, ColumnText, PdfPageEventHelper, PdfWriter}
import multiBible.Interop

class PdfDocument(path: String) {
  private var _pageNumber = 0
  private val _document = new Document()
  val pdfWriter = PdfWriter.getInstance(_document, new FileOutputStream(path))
  pdfWriter.setPageEvent(new _headerFooter)
  _document.open()

  def addParagraph(paragraph: Paragraph, left2Right: Boolean = true): Unit = {
    if (left2Right)
      _document.add(paragraph)
    else {
      paragraph.setAlignment(Element.ALIGN_LEFT)

      val multiColumnText = new MultiColumnText
      multiColumnText.addSimpleColumn(36, PageSize.A4.getWidth-36)
      multiColumnText.setRunDirection(PdfWriter.RUN_DIRECTION_RTL)
      multiColumnText.addElement(paragraph)

      _document.add(multiColumnText)
    }
  }

  def addEmptyLine(): Unit = {
    val paragraph = new Paragraph()
    paragraph.add(" ")
    _document.add(paragraph)
  }

  def numPages: Int = _pageNumber + 1

  def close(): Unit = _document.close()

  private class _headerFooter extends PdfPageEventHelper {
    override def onEndPage(writer: PdfWriter, document: Document): Unit = {
      val licenseText =
        "This pdf was generated using itext - http://itextpdf.com. " +
          "Full source code here: https://bitbucket.org/rminehan/multi-bible/"
      val phrase = new Phrase(licenseText, Interop.timesRomanFontFactory(java.awt.Font.PLAIN)(6))

      ColumnText.showTextAligned(writer.getDirectContent, Element.ALIGN_RIGHT, phrase, 559, 13.5f, 0)

      _pageNumber += 1 // _document.getPageNumber doesn't seem to work properly
    }
  }
}


