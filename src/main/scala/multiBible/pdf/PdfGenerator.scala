package multiBible.pdf

import java.io.File
import java.text.SimpleDateFormat

import com.itextpdf.text._
import multiBible.Interop.FontSize
import multiBible.PassageCache.{VerseReference,Passage}
import multiBible.{Version, Interop}

object PdfGenerator {

  /** Generates an interlinear pdf for the passages passed using text information from the map passed.
    *
    * It uses the smallest number of pages it can for the minimum font size passed.
    * Once it knows how many pages it's allowed to use, it finds the largest font that fits within that number of pages.
    *
    * This method can be used for printing a single version/verses combo - it's the trivial case of an interlinear
    * with just one text.
    */
  def generatePdf(
    minimumFontSize: FontSize,
    passages: Seq[Passage],
    versions: Seq[Version],
    verseMap: Map[VerseReference, String],
    filePathFactory: FontSize=>String
  ): Float = {

    val scratchDir = {
      // Currently I don't know of a way to just generate a pdf in memory, see how many page numbers it has,
      // and then only save it if it's the idea font size.
      // This means a pdf is generated for every file size.
      // Currently they get dumped in a hidden "scratch" dir and the optimal one is moved out and renamed.
      val formatter = new SimpleDateFormat("yyyyMMdd-HHmmss")
      val dateString = formatter.format(new java.util.Date)
      val path = s"generated/.scratch/$dateString"
      new File(path).mkdir()
      path
    }

    val outputFileNameGenerator: FontSize=>String = fontSize => s"$scratchDir/test-$fontSize.pdf"

    def generateTemporaryPdf(fontSize: FontSize): Int =
      generatePdf(fontSize, passages, versions, verseMap, outputFileNameGenerator(fontSize))

    val maxNumPages = generateTemporaryPdf(minimumFontSize)

    var fontSize = minimumFontSize + 0.5f

    while (fontSize <= 23f && generateTemporaryPdf(fontSize) == maxNumPages)
      fontSize += 0.5f

    fontSize -= 0.5f

    // Extract out the one with the best font
    new File(outputFileNameGenerator(fontSize)).renameTo(new File(filePathFactory(fontSize)))

    fontSize
  }

  def generatePdf(
    fontSize: FontSize,
    passages: Seq[Passage],
    versions: Seq[Version],
    verseMap: Map[VerseReference, String],
    path: String
  ): Int = {

    val document = new PdfDocument(path)

    def _generateHeading(heading: String, fontSize: FontSize): Paragraph = {
      val headingParagraph = new Paragraph(heading, Interop.timesRomanFontFactory(Font.BOLD)(15))
      headingParagraph.setAlignment(Element.ALIGN_CENTER)
      headingParagraph
    }

    val isMultiPassage = passages.size > 1

    try {

      if (isMultiPassage)
        document.addParagraph(_generateHeading(
          heading = versionHeading(versions),
          fontSize = 16
        ))

      for (passage <- passages) {

        val heading =
          if (isMultiPassage) {
             document.addEmptyLine()
            _generateHeading(
              heading = passage.toString,
              fontSize = 14
            )
          }
          else
            _generateHeading(
              heading = passageHeading(passage, versions),
              fontSize = 15
            )

        document.addParagraph(heading)

        for (verseNumber <- passage.verseRange) {
          document.addEmptyLine()
          for (version <- versions) {
            val verseReference = VerseReference(version, passage.book, passage.chapter, verseNumber)
            val verseText = verseMap.getOrElse(verseReference, throw new RuntimeException(
              s"Verse $verseNumber missing from version:'${version.description}' while rendering passage '$passage'"
            ))
            version.verseRenderer.renderVerse(document, verseNumber, verseText, fontSize)
          }
        }
      }
      document.numPages
    }
    finally {
      document.close()
    }
  }

  def versionHeading(versions: Seq[Version]): String =
    versions.map(_.description).mkString("/")

  def passageHeading(passage: Passage, versions: Seq[Version]): String =
    passage + " " + versionHeading(versions)
}
