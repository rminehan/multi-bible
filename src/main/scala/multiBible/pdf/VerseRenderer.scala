package multiBible.pdf

trait VerseRenderer {
  def renderVerse(pdfDocument: PdfDocument, verseNumber: Int, verseText: String, fontSize: Float): Unit
}
