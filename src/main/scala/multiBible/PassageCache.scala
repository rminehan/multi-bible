package multiBible

import multiBible.PassageCache._
import multiBible.pdf.PdfGenerator
import utils.Log

import scala.collection.mutable

/** Encapsulates the passage information that has been collected so far across different Bible versions */
class PassageCache {

  private val _processed = mutable.Map[VerseReference, String]()

  /** Returns all passages that can be built for the version passed using the data from the cache */
  def allPassages(version: Version): Iterable[Passage] = {
    val allRecords = _processed.keys.filter {
      case VerseReference(vversion, _, _, _) => version == vversion
    }

    // A chapter block represents all the verses within a particular chapter.
    // There might be multiple contiguous passages within a chapter however, e.g. v1-3 and v10-15.
    val chapterBlocks = allRecords
      .groupBy(verseReference => (verseReference.book, verseReference.chapter))

    chapterBlocks.flatMap { case ((book, chapter), verseReferences) =>
      val sortedVerseNumbers = verseReferences.map(_.verseNumber).toSeq.sorted

      var currentPassageStart: Option[Int] = None
      var lastVerseNumber = sortedVerseNumbers.head-1

      val chapterPassages = scala.collection.mutable.ArrayBuffer[Passage]()

      for (verseNumber <- sortedVerseNumbers) {
        if (currentPassageStart.isEmpty)
          currentPassageStart = Some(verseNumber)

        if (verseNumber != lastVerseNumber+1) {
          chapterPassages += Passage(book, chapter, currentPassageStart.get, lastVerseNumber)
          currentPassageStart = Some(verseNumber)
        }

        lastVerseNumber = verseNumber
      }

      chapterPassages += Passage(book, chapter, currentPassageStart.get, lastVerseNumber)

      chapterPassages.toSeq
    }
  }

  /** Returns an immutable copy of the current cache state */
  def toMap: Map[VerseReference, String] = _processed.toMap
  
  /** Searches the cache for all verses in the chapter passed and builds that into a passage object */
  def buildPassage(chapterReference: ChapterReference): Passage = {
    val verses = processedVerses(chapterReference)

    // ASSUMPTION: All verses between the first and last are defined.
    // The app can't really copy with non-contiguous passages.
    Passage(
      book = chapterReference.book,
      chapter = chapterReference.chapter,
      startVerse = verses.head._1,
      endVerse = verses.last._1
    )
  }
  
  def add(chapterReference: ChapterReference, verses: NumberedVerses): Unit = {
    verses.foreach {
      case (verseNumber, verseText) =>
        val verseReference = chapterReference.verseReference(verseNumber)
        _processed(verseReference) = verseText
        Log.message(s"Added: '$verseReference' -> '$verseText'", indentationLevel = 3)
    }
  }
  
  /** Returns the processed verses for the version, book and chapter passed sorted by verse number */
  def processedVerses(chapterReference: ChapterReference): NumberedVerses = {
    _processed.collect {
      case (VerseReference(version, book, chapter, verseNumber), verseText)
        if version==chapterReference.version &&
           book == chapterReference.book &&
           chapter == chapterReference.chapter  => (verseNumber, verseText)
    }.toSeq.sorted(Ordering.by[(Int,String),Int](_._1))
  }
  
  /** Finds all the verse objects for the head reference across the different versions,
    * then merges them together, verse by verse, into an interlinear form
    * and outputs the result as a string.
    *
    * This is useful for spitting a passage to the clipboard for pasting into a document.
    * The version information is however lost.
    */
  def buildInterlinear(passage: Passage, versions: Seq[Version]): String = {

    val book = passage.book
    val chapter = passage.chapter
    
    val heading = PdfGenerator.passageHeading(passage, versions)

    val interlinearVerseBlocks = (passage.startVerse to passage.endVerse).map {
      case verseNumber =>
        val versionVerses = versions.map {
          case version =>
            val verseReference = VerseReference(version, book, chapter, verseNumber)
            val verseText = _processed.getOrElse(verseReference, throw new RuntimeException(
              s"Verse reference '$book $chapter:$verseNumber' is not defined for version '$version'"
            ))
            s"$verseNumber $verseText"
        }
        versionVerses.mkString("\n")
    }

    s"$heading\n\n${interlinearVerseBlocks.mkString("\n\n")}"
  }

  def clear(): Unit = {
    _processed.clear()
  }
}

object PassageCache {
  type NumberedVerses = Seq[(Int, String)]

  case class Passage(book: Book, chapter: Int, startVerse: Int, endVerse: Int) {
    override def toString: String = {
      val tail =
        if (startVerse > 0) {
          if (startVerse < endVerse)
            s":$startVerse-$endVerse"
          else if (startVerse == endVerse)
            s":$startVerse"
          else
            s":???"
        }
        else ""

      s"${book.presentationText} $chapter$tail"
    }

    lazy val verseRange = startVerse to endVerse

    def verseReferences(version: Version) = verseRange.map {
      case verseNumber => VerseReference(version, book, chapter, verseNumber)
    }
    
    def chapterReference(version: Version): ChapterReference = ChapterReference(version, book, chapter)
  }
  
  /** Represents a particular point in a particular Bible translation - not a span of verses, just one single verse */
  case class VerseReference(version: Version, book: Book, chapter: Int, verseNumber: Int)

  /** Represents a particular chapter in a particular Bible translation */
  case class ChapterReference(version: Version, book: Book, chapter: Int) {
    def verseReference(verseNumber: Int): VerseReference = VerseReference(version, book, chapter, verseNumber)
  }
}
