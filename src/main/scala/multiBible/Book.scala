package multiBible

import utils.Black

/** Encapsulates the information associated with a book of the Bible.
  *
  * This is mainly used for matching names (e.g. matching "first chronicles" to the book 1 Chronicles).
  */
trait Book {
  def presentationText: String
  def isMatch(book: String): Boolean

  def isOldTestament: Boolean = Interop.oldTestamentBooks.contains(this)
  def isNewTestament: Boolean = Interop.newTestamentBooks.contains(this)
}

object Book {
  /** Creates a book which will match book names that match the presentation text or other candidates ignoring
    * case and white space */
  def apply(bookPresentationText: String, otherCandidates: String*): Book = new Book {
    val presentationText: String = bookPresentationText
    
    private val _flattenedCandidates =
      (bookPresentationText :: otherCandidates.toList).map(Black.blacken)
    
    def isMatch(book: String): Boolean = {
      val flattenedBook = Black.blacken(book)
      _flattenedCandidates.contains(flattenedBook)
    }

    override def toString: String = bookPresentationText
  }

  /** Given a book suffix (like "Chronicles" or "Kings"), this creates a book which will match
    * both the numeric form of the book name (e.g. "1 Chronicles") and the long form of the name (e.g. "First Chronicles").
    * Matching is case and white space insensitive
    */
  def first(bookPresentationSuffix: String): Book = apply(
    "1 " + bookPresentationSuffix,
    "First " + bookPresentationSuffix
  )

  def second(bookPresentationSuffix: String): Book = apply(
    "2 " + bookPresentationSuffix,
    "Second " + bookPresentationSuffix
  )

  def third(bookPresentationSuffix: String): Book = apply(
    "3 " + bookPresentationSuffix,
    "Third " + bookPresentationSuffix
  )
}

