package multiBible

import java.net.URLEncoder

import com.itextpdf.text._
import com.itextpdf.text.pdf.BaseFont
import groovy.lang.Closure
import Books._
import multiBible.PassageCache.Passage
import multiBible.pdf.{PdfDocument, VerseRenderer}

object Interop {
  type FontSize = Float
  type FontFactory = FontSize=>Font

  lazy val chineseVerseRenderer: VerseRenderer = left2RightVerseRenderer(
    standardFontFactory("Chinese_DroidSansFallbackFull")
  )

  lazy val englishVerseRenderer: VerseRenderer = left2RightVerseRenderer(
    standardFontFactory("English_LiberationSerif-Regular")
  )

  def timesRomanFontFactory(fontStyle: Int): FontFactory =
    fontSize => new Font(Font.FontFamily.TIMES_ROMAN, fontSize, fontStyle)

  lazy val freeSerifFontFactory: FontFactory = standardFontFactory("FreeSerif")

  def standardFontFactory(fileStem: String): FontFactory = fontFactory(baseFont(s"fonts/$fileStem.ttf"))

  lazy val freeSerifL2RVerseRenderer: VerseRenderer = left2RightVerseRenderer(freeSerifFontFactory)

  def fontFactory(baseFont: BaseFont): FontFactory =
    fontSize => new Font(baseFont, fontSize)

  def baseFont(path: String): BaseFont = BaseFont.createFont(path, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED)

  private def _emptyParagraph(fontSize: Float): Paragraph = {
    val verseParagraph = new Paragraph
    verseParagraph.setKeepTogether(true)
    verseParagraph.setLeading(fontSize * 1.15f)
    verseParagraph
  }

  private def _verseParagraph(fontFactory: FontFactory, verseNumber: Int, verseText: String, fontSize: FontSize): Paragraph = {
    val verseParagraph = _emptyParagraph(fontSize)

    // Verse Number
    verseParagraph.add(new Chunk(verseNumber + " ", timesRomanFontFactory(Font.BOLD)(fontSize)))

    // Verse Text
    verseParagraph.setFont(fontFactory(fontSize))
    for (word <- verseText.split("\\s+"))
      verseParagraph.add(new Chunk(" " + word))

    verseParagraph
  }

  def left2RightVerseRenderer(fontFactory: FontFactory): VerseRenderer = new VerseRenderer {
    def renderVerse(pdfDocument: PdfDocument, verseNumber: Int, verseText: String, fontSize: FontSize): Unit =
      pdfDocument.addParagraph(_verseParagraph(fontFactory, verseNumber, verseText, fontSize), left2Right = true)
  }

  def right2LeftVerseRenderer(fontFactory: FontFactory): VerseRenderer = new VerseRenderer {
    def renderVerse(pdfDocument: PdfDocument, verseNumber: Int, verseText: String, fontSize: FontSize): Unit =
      pdfDocument.addParagraph(_verseParagraph(fontFactory, verseNumber, verseText, fontSize), left2Right = false)
  }

  /** Converts a groovy closure into a scala Function0. */
  def function0[Out](groovyClosure: Closure[Out]): ()=>Out = groovyClosure.call

  /** Converts a groovy closure into a scala Function1.
    *
    * Note that this operation isn't typesafe on the input as closures don't have a way of limiting
    * the input to a specific type.
    * It is assumed that the `In` will make sense when read by the Closure
    */
  def function1[In,Out](groovyClosure: Closure[Out]): In=>Out = groovyClosure.call

  /** Constructs a link for finding the passage on Bible gateway.
    *
    * Verse numbers are currently ignored (as the UI doesn't use them yet so it's needless complexity).
    */
  def bibleGatewayUrl(versionAcronym: String, passage: Passage): String = {
    // Bible Gateway can't handle some book searches (e.g. "First Kings")
    // so we use the standard presentation text (e.g. "1 Kings")
    val passageSearch = encode(passage.toString)
    s"https://www.biblegateway.com/passage/?search=$passageSearch&version=$versionAcronym"
  }

  def encode(string: String): String = URLEncoder.encode(string, "UTF-8")
  
  lazy val oldTestamentBooks = Seq(
    genesis,
    exodus,
    leviticus,
    numbers,
    deuteronomy,
    joshua,
    judges,
    ruth,
    firstSamuel,
    secondSamuel,
    firstKings,
    secondKings,
    firstChronicles,
    secondChronicles,
    ezra,
    nehemiah,
    esther,
    job,
    psalms,
    proverbs,
    ecclesiastes,
    songOfSongs,
    isaiah,
    jeremiah,
    lamentations,
    ezekiel,
    daniel,
    hosea,
    joel,
    amos,
    obadiah,
    jonah,
    micah,
    nahum,
    habakkuk,
    zephaniah,
    haggai,
    zechariah,
    malachi
  )

  lazy val newTestamentBooks = Seq(
    matthew,
    mark,
    luke,
    john,
    acts,
    romans,
    firstCorinthians,
    secondCorinthians,
    galatians,
    ephesians,
    philippians,
    colossians,
    firstThessalonians,
    secondThessalonians,
    firstTimothy,
    secondTimothy,
    titus,
    philemon,
    hebrews,
    james,
    firstPeter,
    secondPeter,
    firstJohn,
    secondJohn,
    thirdJohn,
    jude,
    revelation
  )

  def leftPadWithZeros(number: Int): String =
    number.toString.reverse.padTo(2, '0').reverse

  /** Returns a list of all books of the Bible according to the standard protestant ordering */
  lazy val allBooks = oldTestamentBooks ++ newTestamentBooks

  def isOldTestament(book: Book): Boolean = oldTestamentBooks.contains(book)
  def isNewTestament(book: Book): Boolean = newTestamentBooks.contains(book)
  
  def findBook(bookSearch: String): Book =
    allBooks.find(_.isMatch(bookSearch)).getOrElse(
      throw new RuntimeException(s"Unable to find book '$bookSearch' among list of Bible books")
    )

  /** Returns the book number of a book in the Bible.
    *
    * Note that this is 1-indexed, e.g. Genesis is book 1.
    *
    * For reference, Matthew is book 40 and Revelation is book 66.
    */
  def bookNumber(book: Book): Int = {
    val index = allBooks.indexOf(book)

    if (index == -1)
      throw new RuntimeException(s"Unable to find book '${book.presentationText}' among list of Bible books... Is it apocryphal?")

    // 1-indexed
    index + 1
  }
}
