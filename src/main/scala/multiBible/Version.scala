package multiBible

import multiBible.PassageCache.Passage
import multiBible.pdf.VerseRenderer

trait Version {
  def description: String
  /** A "best attempt" at generating a link based on the credentials passed.
    * Different websites offer different levels of filtering down text.
    * Some like Bible gateway allow you to filter all the way to a verse selection via a link,
    * others might only allow chapter or book.
    */
  def link(passage: Passage): String
  def fileStem: String
  def verseRenderer: VerseRenderer

  override def toString: String = description
}
