package multiBible

import java.awt.{Desktop, GridLayout, BorderLayout}
import java.awt.event.{FocusEvent, FocusListener, ActionEvent, ActionListener}
import java.net.URI
import javax.swing._
import UI._
import multiBible.Interop.FontSize
import multiBible.PassageCache.{VerseReference, Passage, ChapterReference}
import multiBible.pdf.PdfGenerator
import utils._
import scala.util.{Failure, Success}

class UI extends JFrame {
  Log.message("Starting UI")

  private implicit class OptionOps[Data](opt: Option[Data]) {
    def getSafe: Data = opt.getOrElse {
      throw new Exception("Field has not been set yet")
    }

    def foreachMap(sideEffect: Data => Unit): Option[Data] = {
      opt.foreach(sideEffect)
      opt
    }
  }

  private val _passageCache = new PassageCache

  private var _chapterReferenceOpt: Option[ChapterReference] = None
  private var _versionOpt: Option[Version] = None
  private var _bookOpt: Option[Book] = None
  private var _chapterOpt: Option[Int] = None

  private var _workingText = ""

  private def _updateModel[Parsed](userInput: String)(handler: String => Parsed): Option[Parsed] = {
    Option(userInput).filter(_.nonEmpty).map(handler).foreachMap { unused =>
      _updateModel()
    }
  }

  private def _updateModel(): Unit = {
    // TODO - use an applicative here
    (_versionOpt, _bookOpt, _chapterOpt) match {
      case (Some(version), Some(book), Some(chapter)) =>
        _chapterReferenceOpt = Some(ChapterReference(version, book, chapter))
      case _ =>
    }
  }

  getContentPane.add(BorderLayout.NORTH, build[Box] (
    Box.createVerticalBox(),

    // Context hints
    _.add { labelledTextBox("Book", bookText => { _bookOpt = _updateModel(bookText)(Interop.findBook) })},
    _.add { labelledTextBox("Chapter", chapterText => { _chapterOpt = _updateModel(chapterText)(_.toInt) })},
    _.add { labelledTextBox("Verses", versesText => {})}, // This is just a place to record verse numbers, it's not active

    // versions
    _.add { versionRadioButtons(
      version => { _versionOpt = Some(version); _updateModel() },
      _.link(Passage(_chapterReferenceOpt.getSafe.book, _chapterReferenceOpt.getSafe.chapter, -1, -1))
    )},

    // Processing buttons
    _.add { build[Box] (
      Box.createHorizontalBox,
      _.add { button("Process clipboard", 'c', _parseAndStoreVerseText(Clipboard.get)) },
      _.add { button("Process working area", 'w', _parseAndStoreVerseText(_workingText)) }
    )},

    // Hacks panel
    _.add { build[Box](
      Box.createHorizontalBox,
      _.add { new JLabel("Hacks: ") },
      _.add { button("Load BG Passage With Bad Verse 1", 'B', _loadBGPassageWithIncorrectLeadingVerse()) }
    )},

    // Work area where you can edit the text
    _.add { build[JPanel] (
      new JPanel,
      _.setLayout(new BorderLayout),
      _.add(
        new JScrollPane(
          build[JTextArea] (
            new JTextArea,
            _.setRows(20),
            _.setColumns(180),
            _.setLineWrap(true),
            textArea => textArea.addFocusListener(focusListener({ _workingText = textArea.getText }))
          )
        ),
        BorderLayout.CENTER
      )
    )},

    // Button to generate it all
    _.add { build[Box](
      Box.createHorizontalBox,
      _.add { button("Spit", 'S', _spitToClipboard())},
      _.add { button("Generate", 'G', _generate()) },
      _.add { button("Clear cache", 'C', _passageCache.clear()) }
    )}
  ))

  setSize(400, 1200)
  setVisible(true)

  // Prevents the UI thread keeping the main app alive after closing
  setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)

  /** Wrapper method to make builder code clearer */
  def buildAndAdd[Component <: JComponent](component: Component, sideEffects: (Component=>Any)*): Unit =
    add(build(component, sideEffects: _*))

  private def _spitToClipboard(): Unit = {
    _chapterReferenceOpt match {
      case Some(chapterReference @ ChapterReference(version, book, chapter)) =>

        val passage = _passageCache.buildPassage(chapterReference)

        val text =
          if (_chapterReferenceOpt.getSafe.version == bridgeVersion)
            _passageCache.buildInterlinear(passage, Seq(version))
          else
            _passageCache.buildInterlinear(passage, Seq(version, bridgeVersion))

        Clipboard.set(text)
        Log.message(s"Spat to clipboard for version $version")

      case None =>
        throw new Exception("Spitting to clipboard when the model isn't initialized")
    }
  }

  /** Parses the text passed into individual verses, and then allocates records for each verse
    * for the version, book and chapter passed using the parsed verse numbers.
    */
  private def _parseAndStoreVerseText(text: String): Unit = {
    Parser.tryParse(Parser.HinduArabicExtractor, text) match {
      case Success(map) =>  _passageCache.add(_chapterReferenceOpt.getSafe, map.toSeq.sortBy(_._1))
      case Failure(exception) => Log.message(s"Error processing text '$text' - $exception")
    }
  }

  private def _generate(): Unit = {
    val outputDir = new OutputDir
    val verseMap = _passageCache.toMap

    val sermonVersions @ Seq(esv, hebrew, greek) = Seq(
      Plugins.findVersion("ESV"),
      Plugins.findVersion("Hebrew"),
      Plugins.findVersion("SBL")
    )

    val regularInterlinearizedVersions = interlinearVersions.diff(sermonVersions)

    _generateInterlinears(
      bridgeVersion = bridgeVersion,
      interlinearizedVersions = regularInterlinearizedVersions,
      filePathFactory = outputDir.versionFontSizeFactory
    )

    if (_passageCache.allPassages(esv).nonEmpty)
      for (originalLanguageVersion <- Seq(hebrew, greek))
        _generateInterlinear(
          passages = _passageCache.allPassages(originalLanguageVersion).toSeq,
          firstVersion = originalLanguageVersion,
          secondVersion = esv,
          verseMap = verseMap,
          filePathFactory = outputDir.versionFontSizeFactory(originalLanguageVersion)
        )
  }

  private def _generateInterlinears(
    bridgeVersion: Version,
    interlinearizedVersions: Seq[Version],
    filePathFactory: Version => FontSize => String
  ): Unit = {
    // The passages to print are dictated by what has been scraped for the vulgar version.
    val passages = _passageCache.allPassages(bridgeVersion).toSeq
    Log.message(s"Found the following passages: '$passages'")

    val verseMap: Map[VerseReference, String] = _passageCache.toMap

    // Bridge is printed non-interlinear
    PdfGenerator.generatePdf(
      minimumFontSize = _minimumFontSize(bridgeVersion),
      passages = passages,
      versions = Seq(bridgeVersion),
      verseMap = verseMap,
      filePathFactory = filePathFactory(bridgeVersion)
    )

    for (mainVersion <- interlinearizedVersions)
      _generateInterlinear(
        passages = passages,
        firstVersion = mainVersion,
        secondVersion = bridgeVersion,
        verseMap = verseMap,
        filePathFactory = filePathFactory(mainVersion)
      )

    Log.message("Finished generating files")
  }
  
  private def _generateInterlinear(
    passages: Seq[Passage],
    firstVersion: Version,
    secondVersion: Version,
    verseMap: Map[VerseReference, String],
    filePathFactory: FontSize=>String
  ): Unit = {
    // Only generate the version if there are some matching verses defined for this version.
    val generateVersion = _passagesDefinedForVersion(passages, firstVersion, verseMap)

    if (generateVersion) {
      PdfGenerator.generatePdf(
        minimumFontSize = _minimumFontSize(firstVersion),
        passages = passages,
        versions = Seq(firstVersion, secondVersion),
        verseMap = verseMap,
        filePathFactory = filePathFactory
      )

      Log.message(s"Generating interlinear for version '${firstVersion.description}'")
    }
    else
      Log.message(s"Nothing collected for version '${firstVersion.description}' so not generating pdf")
  }

  private def _minimumFontSize(version: Version): FontSize = version.description match {
    case "English" => 18f // Helen likes a large font
    case "Tamil" => 17f // Some Tamil guys have trouble with small fonts
    case _ => 15f
  }

  private def _passagesDefinedForVersion(passages: Seq[Passage], version: Version, verseMap: Map[VerseReference, String]): Boolean =
    passages.flatMap(_.verseReferences(version)).exists(verseMap.contains)

  private def _loadBGPassageWithIncorrectLeadingVerse(): Unit = {
    val passage = Clipboard.get.trim

    if (!passage.startsWith(_chapterOpt.getSafe.toString))
      throw new RuntimeException("Trying to apply a BG hack to a passage that doesn't start with the chapter number")

    val correctedPassage = "1 " + passage.substring(_chapterOpt.getSafe.toString.length)

    _parseAndStoreVerseText(correctedPassage)
  }
}

object UI {
  private val versionInfo: VersionInfo = {
    // Currently the config is assumed to be in path ./versions.yaml in the base directory.
    // This file isn't under version control so that it can be modified each week.
    val fileText = scala.io.Source.fromFile("versions.yaml").mkString
    VersionInfo.parse(fileText)
  }

  lazy val bridgeVersion: Version = versionInfo.bridge

  lazy val interlinearVersions: Seq[Version] = versionInfo.interlinear

  /** Button factory method which wraps up all the java/swing ugliness */
  def button(text: String, mnemonic: Char, callback: => Unit): JButton =
    build[JButton](
      new JButton,
      _.setText(text),
      _.addActionListener(actionListener(_ => callback)),
      _.setMnemonic(mnemonic),
      button => button.setBounds(button.getX, button.getY, 100, 40)
    )

  def radioButton(text: String, selected: Boolean = false): JRadioButton = {
    build[JRadioButton](
      new JRadioButton,
      _.setText(text),
      _.setSelected(selected)
    )
  }
  
  def versionRadioButtons(versionBinding: Version=>Unit, buildLink: Version=>String): JPanel = {
    // The button group is not a graphical entity.
    // It logically groups the buttons so that only one can be selected.
    val buttonGroup = new ButtonGroup

    val panel = new JPanel(new GridLayout(0, 1))

    (bridgeVersion +: interlinearVersions).foreach {
      case version =>
        val radioButton = build[JRadioButton](
          new JRadioButton,
          _.addActionListener(actionListener(_ => versionBinding(version)))
        )

        buttonGroup.add(radioButton)
        panel.add(build[Box](
          Box.createHorizontalBox(),
          _.add(radioButton),
          _.add(link(version.description, buildLink(version)))
        ))
    }

    panel
  }

  def link(text: String, url: =>String): JButton = build[JButton](
    new JButton,
    _.setText(s"""<HTML><FONT color="#000099"><U>$text</U></FONT></HTML>"""),
    _.setBorderPainted(false),
    _.setOpaque(false),
    _.setBackground(java.awt.Color.WHITE),
    _.addActionListener(actionListener(_ =>
      if (Desktop.isDesktopSupported) {
        try {
          Desktop.getDesktop.browse(new URI(url))
        }
        catch {
          case exception: Exception => Log.message(s"Unable to open link '$url', caused by $exception")
        }
      }
    ))

  )

  def build[Component](component: Component, sideEffects: (Component=>Any)*): Component = {
    sideEffects.foreach(sideEffect => sideEffect(component))
    component
  }
  
  def actionListener(callback: ActionEvent=>Unit): ActionListener = new ActionListener {
    def actionPerformed(actionEvent: ActionEvent): Unit = callback(actionEvent)
  }

  def focusListener(callback: =>Unit): FocusListener = new FocusListener {
    override def focusGained(e: FocusEvent): Unit = {}
    override def focusLost(e: FocusEvent): Unit = callback
  }

  def labelledTextBox(label: String, binding: String=>Unit): Box = build[Box] (
    Box.createHorizontalBox(),

    _.add { build[JLabel](
      new JLabel,
      _.setText(label),
      _.setBounds(10, 10, 60, 10)
    )},

    _.add { build[JTextField](
      new JTextField,
      textField => textField.addFocusListener(focusListener(binding(textField.getText)))
    )}
  )
}
