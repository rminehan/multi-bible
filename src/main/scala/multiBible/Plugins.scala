package multiBible

import utils.Black

object Plugins {
  lazy val versions: Iterable[Version] = {
    val currentJavaClassLoader = getClass.getClassLoader
    import groovy.lang.GroovyClassLoader
    val groovyClassLoader = new GroovyClassLoader(currentJavaClassLoader)

    val pluginsDir = new java.io.File("plugins")

    if (pluginsDir.exists) {
      pluginsDir
        .listFiles
        .filter(!_.getName.startsWith("."))
        .filter(_.getName.endsWith(".groovy"))
        .map(groovyClassLoader.parseClass(_).newInstance)
        .map {
          case version: Version => version
          case notversion => throw new Exception("A non-version groovy plugin is in the plugins dir")
        }
    }
    else Iterable.empty
  }

  def findVersion(descriptionPrefix: String): Version =
    Plugins
      .versions
      .find(version => Black.startsWith(descriptionPrefix)(version.description) || Black.endsWith(descriptionPrefix)(version.description))
      .getOrElse {
        throw new RuntimeException(s"Aint no version with description matching '$descriptionPrefix' and there never was!")
      }

  lazy val niv = findVersion("niv")
  lazy val greek = findVersion("sbl")
  lazy val hebrew = findVersion("hebrew")
}
