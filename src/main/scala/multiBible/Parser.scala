package multiBible

import java.util.regex.{Matcher, Pattern}

import scala.util.{Success, Failure, Try}
import scala.collection.mutable

object Parser {

  trait NumericExtractor {
    def matcher(text: String): Matcher
    def extractNumber(text: String, matcher: Matcher): Int
  }

  object HinduArabicExtractor extends NumericExtractor {
    def matcher(text: String): Matcher = Pattern.compile("\\b\\d+\\b").matcher(text)
    def extractNumber(text: String, matcher: Matcher): Int = text.substring(matcher.start, matcher.end).toInt
  }

  object DariExtractor extends NumericExtractor {
    lazy val dari2HinduArabic = Map(
      '۰' -> '0',
      '۱' -> '1',
      '۲' -> '2',
      '۳' -> '3',
      '۴' -> '4',
      '۵' -> '5',
      '۶' -> '6',
      '۷' -> '7',
      '۸' -> '8',
      '۹' -> '9'
    )

    lazy val dariPattern = Pattern.compile(s"\\b[${dari2HinduArabic.keys.mkString}]+")
    
    def matcher(text: String): Matcher = dariPattern.matcher(text)
    def extractNumber(text: String, matcher: Matcher): Int =
      text.substring(matcher.start, matcher.end).map {
        case dariChar => dari2HinduArabic(dariChar)
      }.toInt
  }

  def tryParse(numericExtractor: NumericExtractor, text: String): Try[Map[Int, String]] = {
    val map = mutable.Map[Int, String]()
    val cleanedText = text.trim

    val matcher = numericExtractor.matcher(cleanedText)

    if (matcher.find()) {
      if (matcher.start != 0) {
        return Failure(new RuntimeException(s"Text '${cleanedText.substring(0, matcher.start)}' found before first verse reference"))
      }

      var currentVerseNumberBeingProcessed = numericExtractor.extractNumber(cleanedText, matcher)
      var verseTextStart = matcher.end

      def addToMap(endPoint: Int): Unit =
        map(currentVerseNumberBeingProcessed) = cleanedText.substring(verseTextStart, endPoint).trim

      while (matcher.find) {
        val nextNumber = numericExtractor.extractNumber(cleanedText, matcher)
        if (nextNumber == currentVerseNumberBeingProcessed + 1) {
          addToMap(matcher.start)
          currentVerseNumberBeingProcessed = nextNumber
          verseTextStart = matcher.end
        }
      }

      // Clean up the trailing stuff after the last match
      addToMap(cleanedText.length)
    }

    Success(map.toMap)
  }
}
