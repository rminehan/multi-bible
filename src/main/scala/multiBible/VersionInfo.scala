package multiBible

import org.yaml.snakeyaml.Yaml
import scala.collection.JavaConverters._

import java.util.{ArrayList, LinkedHashMap}

case class VersionInfo(bridge: Version, interlinear: Seq[Version])

object VersionInfo {
  def parse(yaml: String): VersionInfo = {
    val versionInfoYaml = (new Yaml).load(yaml).asInstanceOf[LinkedHashMap[Any, Any]]

    val bridgeVersion = {
      val bridgeText = versionInfoYaml.get("bridge").asInstanceOf[String]
      Plugins.versions.find(_.description == bridgeText).getOrElse(
        throw new RuntimeException(s"Bridge version '$bridgeText' couldn't be found")
      )
    }

    val interlinearVersions: Seq[Version] =
      versionInfoYaml.get("interlinear").asInstanceOf[ArrayList[String]].asScala.map(Plugins.findVersion)

    VersionInfo(bridgeVersion, interlinearVersions)
  }
}
