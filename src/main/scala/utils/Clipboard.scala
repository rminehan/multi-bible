package utils

import java.awt.Toolkit
import java.awt.datatransfer.{StringSelection, DataFlavor}

object Clipboard {
  def get: String =
    Toolkit
      .getDefaultToolkit
      .getSystemClipboard
      .getData(DataFlavor.stringFlavor)
      .asInstanceOf[String]

  def set(text: String): Unit =
    Toolkit
      .getDefaultToolkit
      .getSystemClipboard
      .setContents(new StringSelection(text), null)
}

