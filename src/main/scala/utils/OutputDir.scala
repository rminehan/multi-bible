package utils

import java.io.File
import java.text.SimpleDateFormat

import multiBible.Interop.FontSize
import multiBible.Version
import OutputDir._

/** When constructed, this creates a timestamped folder for saving pdf files into.
  *
  * It has convenience methods for then returning paths within that folder.
  */
class OutputDir {
  val dirPath = {
    val formatter = new SimpleDateFormat("yyyyMMdd-HHmmss")
    val dateString = formatter.format(new java.util.Date)
    val dir = s"$baseGeneratedDir/$dateString"
    new File(dir).mkdir()
    dir
  }

  def versionFontSizeFactory: Version=>FontSize=>String =
    version => fontSize => path(s"${version.fileStem}-$fontSize.pdf")

  def path(filename: String): String = s"$dirPath/$filename"
}

object OutputDir {
  val baseGeneratedDir = "generated"

  /** Returns a path to the last generated output dir */
  def last: String = {
    val dateFormatRegex = "^" + "\\d"*8 + "-" + "\\d"*6 + "$"
    new File(baseGeneratedDir)
      .listFiles()
      .filter(_.isDirectory)
      .filter(_.getName.matches(dateFormatRegex))
      .map(_.getAbsolutePath)
      .sorted
      .last
  }
}
