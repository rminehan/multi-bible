package utils

object Black {
  def equals(left: String, right: String): Boolean = blacken(left) == blacken(right)

  def startsWith(inner: String): String=>Boolean =
    test => blacken(test).startsWith(blacken(inner))

  def endsWith(inner: String): String=>Boolean =
    test => blacken(test).endsWith(blacken(inner))

  def blacken(text: String): String = text.toLowerCase.replaceAll("\\s", "")
}
