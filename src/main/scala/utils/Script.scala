package utils

import scala.util.{Try, Success, Failure}

object Script {
  /**
   * Finds the value associated with a flag.
   * e.g. if "-g generated" is defined, passing "g" to this method will return "generated"
   */
  def flagValue(flag: String)(implicit args: Array[String]): String = {
    tryFlagValue(flag) match {
      case Success(value) => value
      case Failure(exception) => throw exception
    }
  }

  def tryFlagValue(flag: String)(implicit args: Array[String]): Try[String] = {
    args.indexWhere(arg => Black.equals(arg, s"-$flag")) match {
      case -1 => Failure(new RuntimeException(s"Missing compulsory command line flag: '$flag'"))
      case index =>
        if (index + 1 >= args.size || args(index+1).startsWith("-"))
          Failure(new IllegalArgumentException(s"Flag '$flag' defined but no value after"))
        else
          Success(args(index+1))
    }
  }

  /**
    * Determines whether a flag exists in the input args.
    * Doesn't care about whether the flag has an associated value.
   */
  def flagDefined(flag: String)(implicit args: Array[String]): Boolean = args.exists(arg => Black.equals(arg, "-" + flag))
}
