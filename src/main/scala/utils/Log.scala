package utils

object Log {
  def message(message: String, indentationLevel: Int = 0, prefix: String = "* "): Unit = {
    val threadInfo = if (displayThreadInfo) s"[${Thread.currentThread().getName}] " else ""
    val indent = " " * indentationLevel
    println(threadInfo + indent + prefix + message)
  }

  lazy val displayThreadInfo: Boolean = Option(System.getenv("displayThreadInfo")).exists(Black.equals(_, "true"))
}
