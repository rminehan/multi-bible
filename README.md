# Multi Bible

A tool for generating interlinear pdf's of Bible passages.

# The backstory

St Phil's Auburn has an evening service with lots of people from different countries, many with weak English.

The teaching is generally done in English, but I thought it would be helpful for non-English speakers to have an interlinear printout of the passage in their own language so that they can follow along verse by verse.

# Concepts

A "bridge" version - this is the Bible version that the teaching is done in. It's usually an English translation of the Bible like the CEV, ESV or NIV.
The bridge version is what gets interleaved into the other versions.

"Interlinear" versions - these are the other Bible versions to be interleaved with the English version.

# Generating pdf's

## Setting up the versions

The first step is determining what bridge and interlinear versions you want. That goes into the versions.yaml file (git ignored). An example might be:

```yaml
bridge: "CEV"

interlinear:
    - "Arabic"
    - "Persian"
    - "Urdu"
    - "Hindi"
    - "Punjabi"
```

## Launching the UI

Do `sbt run` and then select the `UI` app.

This will cause a UI to appear (an old school java swing app).

## Building pdf's

The UI is a working area where you copy text in and then "process" it.

First enter the book and chapter number (passages crossing chapter boundaries probably aren't supported).

If you're doing a whole chapter you can leave the verse area blank.

The flow for entering data is that you select the version you want to enter data for, then either:

- put those verses onto your clipboard and click "Process clipboard"
- put those verses into the working area (the text box) and click "Process working area"

The app parses the text into individual verses and sticks it into a little in-memory data store.

When you're finished loading all the data in you click "Generate" and it creates pdf's for all the versions in the list that it can find data for in a subdirectory of the `generated` folder.

## Tips

The text for each version acts like a hyperlink and (if working) can take you to a webpage with the Bible passage.

There is also a button "Load BG Passage With Bad Verse 1".
The backstory to this is that Bible Gateway often uses the _chapter_ number in the verse number location for verse 1 of passages, e.g. for Genesis 3 CEV:

```
3 The snake was sneakier than any of the other wild animals that the Lord God had made. One day it came to the woman and asked, “Did God tell you not to eat fruit from any tree in the garden?”

2 The woman answered, “God said we could eat fruit from any tree in the garden,

3 except the one in the middle. He told us not to eat fruit from that tree or even to touch it. If we do, we will die.”
```

This would confuse the parser into thinking it starts with v3 and then it would fail when the next verse is 2.

Here you could copy it into the working area and manually edit it, or you could click that button instead and it understands it's from Bible Gateway and starts at v1.

#  Plugins

Each Bible "version" is represented by a Groovy file which implements the logic for that version.

Logic includes things like:

- whether the text is left to right or right to left
- what filename to give pdf's corresponding to the version
- what kind of font to render the text in
- how to generate a link to an online Bible using for a particular Bible verse (this powers the hyperlink functionality)

The version names in the `versions.yaml` above correspond to the `description` function in those scripts.

The majority of the versions are powered by Bible Gateway and generally to make a new one just copy and paste an existing one with text that runs in the same direction then modify the obvious fields.

# TODO

Make the `generated` and `generated/.scratch` dirs get created automatically.
