import multiBible.Version
import multiBible.PassageCache.Passage
import multiBible.Interop
import multiBible.pdf.VerseRenderer

public class SBL implements Version {
    @Override
    String description() {
        "Greek SBL"
    }

    @Override
    String link(Passage passage) {
        if (passage.book().oldTestament)
            throw new RuntimeException("SBT is only provided for the New Testament (the Old Testament isn't in Greek)")
        Interop.bibleGatewayUrl("SBLGNT", passage)
    }

    @Override
    String fileStem() {
        "sbl"
    }

    @Override
    VerseRenderer verseRenderer() {
        Interop.freeSerifL2RVerseRenderer()
    }
}

