import multiBible.Books
import multiBible.Version
import multiBible.PassageCache.Passage
import multiBible.Interop
import multiBible.pdf.VerseRenderer

public class TCAV implements Version {
    @Override
    String description() {
        "Tim Cock's Authorised Version"
    }

    @Override
    String link(Passage passage) {
        return "See Pastor Tim"
    }


    @Override
    String fileStem() {
        "tcav"
    }

    @Override
    VerseRenderer verseRenderer() {
        Interop.englishVerseRenderer()
    }
}


