import multiBible.Interop
import multiBible.Version
import multiBible.PassageCache.Passage
import multiBible.pdf.VerseRenderer

class Korean implements Version {
    @Override
    String description() {
        "Korean"
    }

    @Override
    String link(Passage passage) {
        int bookNumber = Interop.bookNumber(passage.book())

        String bookNumberText = Interop.leftPadWithZeros(bookNumber)
        String chapterText = passage.chapter()

        "http://www.wordplanet.org/kr/${bookNumberText}/${chapterText}.htm"
    }

    @Override
    String fileStem() {
        "korean"
    }

    @Override
    VerseRenderer verseRenderer() {
        Interop.left2RightVerseRenderer(
            Interop.fontFactory(Interop.baseFont("fonts/Korean_NanumGothic.ttf"))
        )
    }
}
