import multiBible.Interop
import multiBible.Version
import multiBible.PassageCache.Passage
import multiBible.pdf.VerseRenderer

// TODO - dedupe with CNVT
class Chinese_CNVS implements Version {
    @Override
    String description() {
        "CNVS"
    }

    @Override
    String link(Passage passage) {
        Interop.bibleGatewayUrl("CNVS", passage)
    }

    @Override
    String fileStem() {
        "chinese_cnv_simplified"
    }

    @Override
    VerseRenderer verseRenderer() {
        Interop.chineseVerseRenderer()
    }
}
