import multiBible.Interop
import multiBible.Version
import multiBible.PassageCache.Passage
import multiBible.pdf.VerseRenderer

class Turkish implements Version {
    @Override
    String description() {
        "Turkish"
    }

    @Override
    String link(Passage passage) {
        // TODO - add more specific links for individual books.
        // Note that each book is an entire page!
        "https://www.sacred-texts.com/bib/wb/trk/index.htm"
    }

    @Override
    String fileStem() {
        "turkish"
    }

    @Override
    VerseRenderer verseRenderer() {
        Interop.left2RightVerseRenderer(Interop.freeSerifFontFactory())
    }
}
