import multiBible.Interop
import multiBible.Version
import multiBible.PassageCache.Passage
import multiBible.pdf.VerseRenderer

class Hindi implements Version {
    @Override
    String description() {
        "Hindi Easy-to-Read"
    }

    @Override
    String link(Passage passage) {
        Interop.bibleGatewayUrl("ERV-HI", passage)
    }

    @Override
    String fileStem() {
        "hindi"
    }

    @Override
    VerseRenderer verseRenderer() {
        Interop.left2RightVerseRenderer(Interop.freeSerifFontFactory())
    }
}
