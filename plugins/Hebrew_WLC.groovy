import multiBible.Version
import multiBible.PassageCache.Passage
import multiBible.Interop
import multiBible.pdf.VerseRenderer

public class Hebrew_WLC implements Version {
    @Override
    String description() {
        "Hebrew Westminster Leningrad Codex"
    }

    @Override
    String link(Passage passage) {
        if (passage.book().newTestament)
            throw new RuntimeException("WLC is only provided for the Old Testament (the New Testament isn't in Hebrew - well we don't have any documents although some people argue that they were originally written in Hebrew and then translated into Koine Greek)")
        Interop.bibleGatewayUrl("WLC", passage)
    }

    @Override
    String fileStem() {
        "hebrew_wlc"
    }

    @Override
    VerseRenderer verseRenderer() {
        Interop.right2LeftVerseRenderer(Interop.freeSerifFontFactory())
    }
}

