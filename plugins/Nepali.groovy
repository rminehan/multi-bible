import multiBible.Interop
import multiBible.Version
import multiBible.PassageCache.Passage
import multiBible.pdf.VerseRenderer

class Nepali_ERV implements Version {
    @Override
    String description() {
        "Nepali Easy-to-Read"
    }

    @Override
    String link(Passage passage) {
        Interop.bibleGatewayUrl("ERV-NE", passage)
    }

    @Override
    String fileStem() {
        "nepali"
    }

    @Override
    VerseRenderer verseRenderer() {
        Interop.left2RightVerseRenderer(Interop.freeSerifFontFactory())
    }
}
