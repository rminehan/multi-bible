import multiBible.Interop
import multiBible.Version
import multiBible.PassageCache.Passage
import multiBible.pdf.VerseRenderer

class Bengali implements Version {
    @Override
    String description() {
        "Bengali"
    }

    @Override
    String link(Passage passage) {
        // Books are pdf's
        "https://www.bibleleague.org/bible-downloads/"
    }

    @Override
    String fileStem() {
        "bengali"
    }

    @Override
    VerseRenderer verseRenderer() {
        Interop.left2RightVerseRenderer(Interop.freeSerifFontFactory())
    }
}

/*
 Note the Bible we found uses its own numbers, not hindu-arabic so you need to fix them verse by verse.
 ০ - 0
 ১ - 1
 ২ - 2
 ৩ - 3
 ৪ - 4
 ৫ - 5
 ৬ - 6
 ৭ - 7
 ৮ - 8
 ৯ - 9

 Also the unicode seems quite exotic and doesn't render correctly in vim or in the generated pdf's.
 Potentially the pdf we're copying it out of is using a different font system.
 Sadly this version is not usable in its current state.
 */
