import multiBible.Version
import multiBible.PassageCache.Passage
import multiBible.Interop
import multiBible.pdf.VerseRenderer

public class ESV implements Version {
    @Override
    String description() {
        "ESV"
    }

    @Override
    String link(Passage passage) {
        Interop.bibleGatewayUrl("ESV", passage)
    }

    @Override
    String fileStem() {
        "esv"
    }

    @Override
    VerseRenderer verseRenderer() {
        Interop.englishVerseRenderer()
    }
}

