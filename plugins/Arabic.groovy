import multiBible.Interop
import multiBible.Version
import multiBible.PassageCache.Passage
import multiBible.pdf.VerseRenderer

class Arabic implements Version {
    @Override
    String description() {
        "Arabic"
    }

    @Override
    String link(Passage passage) {
        Interop.bibleGatewayUrl("ERV-AR", passage)
    }

    @Override
    String fileStem() {
        "arabic"
    }

    @Override
    VerseRenderer verseRenderer() {
        Interop.right2LeftVerseRenderer(Interop.freeSerifFontFactory())
    }
}
