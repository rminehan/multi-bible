import multiBible.Interop
import multiBible.Version
import multiBible.PassageCache.Passage
import multiBible.pdf.VerseRenderer

class CEV implements Version {
    @Override
    String description() {
        "CEV"
    }

    @Override
    String link(Passage passage) {
        Interop.bibleGatewayUrl("CEV", passage)
    }

    @Override
    String fileStem() {
        "cev"
    }

    @Override
    VerseRenderer verseRenderer() {
        Interop.left2RightVerseRenderer(Interop.freeSerifFontFactory())
    }
}
