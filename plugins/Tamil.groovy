import multiBible.Interop
import multiBible.Version
import multiBible.PassageCache.Passage
import multiBible.pdf.VerseRenderer

/** A different tamil version because the tamil guys didn't like Tamil ERV.
 *
 * This one is from http://www.tamil-bible.com/ but I don't know what version it is.
 */
class Tamil implements Version {
    @Override
    String description() {
        "Tamil"
    }

    @Override
    String link(Passage passage) {
        String book = passage.book().presentationText().capitalize()
        String chapter = passage.chapter()
        "http://www.tamil-bible.com/lookup.php?Book=$book&Chapter=$chapter&Verse=&Kjv=0"
    }

    @Override
    String fileStem() {
        "tamil"
    }

    @Override
    VerseRenderer verseRenderer() {
        Interop.left2RightVerseRenderer(Interop.freeSerifFontFactory())
    }

    /** Currently verse info isn't sent from the UI.
     * When it is, this can be inserted into the url as a verse filter.
     */
    private String verseRange(Passage passage) {
        int startVerse = passage.startVerse()
        int endVerse = passage.endVerse()
        if (startVerse == endVerse) return "$startVerse"
        else return "$startVerse-$endVerse"
    }
}
