import multiBible.Books
import multiBible.Version
import multiBible.PassageCache.Passage
import multiBible.Interop
import multiBible.pdf.VerseRenderer

public class NIV11 implements Version {
    @Override
    String description() {
        "NIV11"
    }

    @Override
    String link(Passage passage) {
        Interop.bibleGatewayUrl("NIV", passage)
    }


    @Override
    String fileStem() {
        "niv11"
    }

    @Override
    VerseRenderer verseRenderer() {
        Interop.englishVerseRenderer()
    }
}

