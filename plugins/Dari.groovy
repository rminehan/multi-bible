import multiBible.Interop
import multiBible.Version
import multiBible.PassageCache.Passage
import multiBible.pdf.VerseRenderer

class Dari implements Version {
    @Override
    String description() {
        "Dari"
    }

    @Override
    String link(Passage passage) {
        String bookPath = passage.book().presentationText().replace(' ', '-').toLowerCase()
        String chapterPath = "${bookPath}-${passage.chapter()}"

        "http://afghanbibles.org/eng/dari-bible/$bookPath/$chapterPath"
    }

    @Override
    String fileStem() {
        "dari"
    }

    @Override
    VerseRenderer verseRenderer() {
        Interop.right2LeftVerseRenderer(Interop.freeSerifFontFactory())
    }
}

