import multiBible.Books
import multiBible.Version
import multiBible.PassageCache.Passage
import multiBible.Interop
import multiBible.pdf.VerseRenderer

public class NIV11 implements Version {
    @Override
    String description() {
        "Holman"
    }

    @Override
    String link(Passage passage) {
        Interop.bibleGatewayUrl("HCSB", passage)
    }


    @Override
    String fileStem() {
        "holman"
    }

    @Override
    VerseRenderer verseRenderer() {
        Interop.englishVerseRenderer()
    }
}


