import multiBible.Interop
import multiBible.Version
import multiBible.PassageCache.Passage
import multiBible.pdf.VerseRenderer

class Chinese_CNVT implements Version {
    @Override
    String description() {
        "CNVT"
    }

    @Override
    String link(Passage passage) {
        Interop.bibleGatewayUrl("CNVT", passage)
    }

    @Override
    String fileStem() {
        "chinese_cnv_traditional"
    }

    @Override
    VerseRenderer verseRenderer() {
        Interop.chineseVerseRenderer()
    }
}

