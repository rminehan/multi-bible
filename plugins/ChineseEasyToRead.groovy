import multiBible.Interop
import multiBible.Version
import multiBible.PassageCache.Passage
import multiBible.pdf.VerseRenderer

class ChineseEasyToRead implements Version {
    @Override
    String description() {
        "Chinese ERV"
    }

    @Override
    String link(Passage passage) {
        // Easy to read Bible is only supported for New Testament
        if (Interop.isOldTestament(passage.book()))
            throw new RuntimeException("Chinese Easy-to-Read hasn't been translated for the Old Testament")
        else {
            Interop.bibleGatewayUrl("ERV-ZH", passage)
        }
    }

    @Override
    String fileStem() {
        "chinese-erv"
    }

    @Override
    VerseRenderer verseRenderer() {
        Interop.chineseVerseRenderer()
    }
}
