import multiBible.Interop
import multiBible.Version
import multiBible.PassageCache.Passage
import multiBible.pdf.VerseRenderer

class Singhalese implements Version {
    @Override
    String description() {
        "Singhalese"
    }

    @Override
    String link(Passage passage) {
        if (passage.book().isOldTestament())
            throw new RuntimeException("Singhalese Bible doesn't have translations for the Old Testament")

        int bookNumber = Interop.bookNumber(passage.book()) + 1
        String bookPrefix = passage.book().presentationText().substring(0, 3).toUpperCase()

        "http://bsceylon.org/sinhala_bible/CBS/NT/SNU-${bookNumber}${bookPrefix}/b${bookNumber}_Chapter${passage.chapter()}.html"
    }

    @Override
    String fileStem() {
        "singhalese"
    }

    @Override
    VerseRenderer verseRenderer() {
        Interop.left2RightVerseRenderer(Interop.freeSerifFontFactory())
    }
}
