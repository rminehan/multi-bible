import multiBible.Interop
import multiBible.Version
import multiBible.PassageCache.Passage
import multiBible.pdf.VerseRenderer

class Urdu implements Version {
    @Override
    String description() {
        "Urdu Easy-to-read"
    }

    @Override
    String link(Passage passage) {
        Interop.bibleGatewayUrl("ERV-UR", passage)
    }

    @Override
    String fileStem() {
        "urdu"
    }

    @Override
    VerseRenderer verseRenderer() {
        Interop.right2LeftVerseRenderer(
            Interop.freeSerifFontFactory()
        )
    }
}
