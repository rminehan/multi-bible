import multiBible.Interop
import multiBible.Version
import multiBible.PassageCache.Passage
import multiBible.pdf.VerseRenderer

class Punjabi implements Version {
    @Override
    String description() {
        "Punjabi Easy-to-Read"
    }

    @Override
    String link(Passage passage) {
        Interop.bibleGatewayUrl("ERV-PA", passage)
    }

    @Override
    String fileStem() {
        "punjabi"
    }

    @Override
    VerseRenderer verseRenderer() {
        Interop.left2RightVerseRenderer(Interop.freeSerifFontFactory())
    }
}
