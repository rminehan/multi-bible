import multiBible.Books
import multiBible.Version
import multiBible.PassageCache.Passage
import multiBible.Interop
import multiBible.pdf.VerseRenderer

public class NIV84 implements Version {
    @Override
    String description() {
        "NIV84"
    }

    @Override
    String link(Passage passage) {
        String bookSearch = passage.book().presentationText()

        if (passage.book() == Books.songOfSongs())
            bookSearch = "song of solomon"

        bookSearch = bookSearch.replace(' ', '-').toLowerCase()

        "http://www.biblestudytools.com/$bookSearch/${passage.chapter()}.html"
    }

    @Override
    String fileStem() {
        "niv84"
    }

    @Override
    VerseRenderer verseRenderer() {
        Interop.englishVerseRenderer()
    }
}