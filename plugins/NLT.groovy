import multiBible.Books
import multiBible.Version
import multiBible.PassageCache.Passage
import multiBible.Interop
import multiBible.pdf.VerseRenderer

public class NIV11 implements Version {
    @Override
    String description() {
        "NLT"
    }

    @Override
    String link(Passage passage) {
        Interop.bibleGatewayUrl("NLT", passage)
    }


    @Override
    String fileStem() {
        "nlt"
    }

    @Override
    VerseRenderer verseRenderer() {
        Interop.englishVerseRenderer()
    }
}


