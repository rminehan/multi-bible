import multiBible.Interop
import multiBible.Version
import multiBible.PassageCache.Passage
import multiBible.pdf.VerseRenderer

class Tamil_ERV implements Version {
    @Override
    String description() {
        "Tamil Easy-to-Read"
    }

    @Override
    String link(Passage passage) {
        Interop.bibleGatewayUrl("ERV-TA", passage)
    }

    @Override
    String fileStem() {
        "tamil"
    }

    @Override
    VerseRenderer verseRenderer() {
        Interop.left2RightVerseRenderer(Interop.freeSerifFontFactory())
    }
}
