import multiBible.Interop
import multiBible.Version
import multiBible.PassageCache.Passage
import multiBible.pdf.VerseRenderer

class Persian implements Version {
    @Override
    String description() {
        "Persian"
    }

    @Override
    String link(Passage passage) {
        String bookNumber = Interop.leftPadWithZeros(Interop.bookNumber(passage.book()))
        "https://www.wordproject.org/bibles/fa/${bookNumber}/${passage.chapter()}.htm"
    }

    @Override
    String fileStem() {
        "persian"
    }

    @Override
    VerseRenderer verseRenderer() {
        Interop.right2LeftVerseRenderer(
            Interop.fontFactory(Interop.baseFont("fonts/Persian_DejaVuSans.ttf"))
        )
    }
}
