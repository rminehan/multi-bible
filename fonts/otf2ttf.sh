#!/usr/local/bin/fontforge
# Quick and dirty hack: converts a font to truetype (.ttf)
# From http://www.stuermer.ch/blog/convert-otf-to-ttf-font-on-ubuntu.html
# Assumes you've installed `fontforge`
# The script isn't standalone, it's used *by* fontforge. Usage:
#    fontforge -script otf2ttf.sh convertMe.otf
Print("Opening "+$1);
Open($1);
Print("Saving "+$1:r+".ttf");
Generate($1:r+".ttf");
Quit(0);
